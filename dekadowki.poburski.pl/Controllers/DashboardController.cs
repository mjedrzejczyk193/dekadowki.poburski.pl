﻿using dekadowki.poburski.pl.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace dekadowki.poburski.pl.Controllers
{
    public class DashboardController : Controller
    {
        List<Budowa> budowy = Help.GetBudowy();

        // GET: Dashboard
        public ActionResult Index()
        {
            //802 - xeos
            var xeos = budowy.Where(b => b.Numer == "802").First();
            xeos.Nazwa = "Xeos " + xeos.Nazwa;

            var currentBudowa = budowy[0];
            if (Session["Budowa"] != null)
            {
                currentBudowa = (Budowa)Session["Budowa"];
            }

            Tydzien currentWeek = Session["Tydzien"] != null ? (Tydzien)Session["Tydzien"] : null;

            try
            {
                if (currentWeek == null)
                {
                    currentWeek = Help.GetTydzien(Convert.ToInt32(Response.Cookies["Tydzien"].Value));
                }
            }
            catch (Exception ex)
            {
                currentWeek = Help.GetTydzien(Help.GetCurrentWeek());
                Debug.WriteLine(ex.Message);
            }

            try
            {
                DashboardViewModel dvm = new DashboardViewModel();
                dvm.Brygada = Session["NowaBrygada"] != null ? (List<Brygada>)Session["NowaBrygada"] : null;
                dvm.DniTygodnia = Help.GetDniTygodnia(dvm.Brygada.Count, currentWeek, dvm.Brygada[0].Id, dvm.Brygada, currentBudowa.Id);
                dvm.Tydzien = currentWeek;
                dvm.Brygadzista = dvm.Brygada[0];
                ViewBag.Message = "";
                ViewBag.ColorMessage = "";
                ViewBag.Budowy = budowy;
                ViewBag.Podpisy = Help.GetPodpisy(dvm, currentBudowa);
                ViewBag.Tydzien = currentWeek;

                //Sessions
                Session["Tydzien"] = currentWeek;
                Session["DVM"] = dvm;
                Session["Budowa"] = currentBudowa;

                //Cookie
                Response.Cookies["Tydzien"].Value = currentWeek.getRokTydzien();
                Response.Cookies["Tydzien"].Expires = DateTime.Now.AddMinutes(15);

                return View(dvm);
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
                return RedirectToAction("Index", "Home");
            }
        }

        public ActionResult Index2(string message, string colorMessage, string serwis, bool? drukuj)
        {
            var currentBudowa = budowy[0];
            if (Session["Budowa"] != null)
            {
                currentBudowa = (Budowa)Session["Budowa"];
            }

            Tydzien currentWeek = Help.GetTydzien(Help.GetCurrentWeek());
            if (Session["Tydzien"] != null)
            {
                currentWeek = (Tydzien)Session["Tydzien"];
            }
            else
            {
                try
                {
                    currentWeek = Help.GetTydzien(Response.Cookies["Tydzien"].Value);
                }
                catch (Exception e)
                {
                    Debug.WriteLine(e.Message);
                    currentWeek = Help.GetTydzien(Help.GetCurrentWeek());
                }
            }

            DashboardViewModel dvm = new DashboardViewModel();
            dvm.Brygada = Help.GetBrygada(Help.GetIdIntranetFromDb(User.Identity.Name), true);
            if (Session["NowaBrygada"] != null)
            {
                dvm.Brygada = (List<Brygada>)Session["NowaBrygada"];
            }
            dvm.DniTygodnia = Help.GetDniTygodnia(dvm.Brygada.Count, currentWeek, dvm.Brygada[0].Id, dvm.Brygada, currentBudowa.Id);
            dvm.Tydzien = currentWeek;
            dvm.Brygadzista = dvm.Brygada[0];
            ViewBag.Message = string.IsNullOrEmpty(message) ? "" : message;
            ViewBag.ColorMessage = string.IsNullOrEmpty(colorMessage) ? "" : colorMessage;
            ViewBag.Budowy = budowy;
            ViewBag.WybranaBudowa = currentBudowa;
            ViewBag.Serwis = string.IsNullOrEmpty(serwis) ? null : serwis;
            ViewBag.Podpisy = Help.GetPodpisy(dvm, currentBudowa);
            ViewBag.Print = drukuj;

            //Sessions
            Session["Tydzien"] = currentWeek;
            Session["DVM"] = dvm;
            Session["Budowa"] = currentBudowa;

            if (currentWeek != null)
            {
                return View("Index", dvm);
            }
            return RedirectToAction("Index", "Home");
        }

        public ActionResult Print()
        {
            var wybranaBudowa = (Budowa)Session["Budowa"];
            ViewBag.WybranaBudowa = wybranaBudowa;
            ViewBag.Tydzien = (Tydzien)Session["Tydzien"];
            DashboardViewModel dvm = (DashboardViewModel)Session["DVM"];
            ViewBag.Podpisy = Help.GetPodpisy(dvm, wybranaBudowa);

            return View(dvm);
        }

        [HttpPost]
        public ActionResult SaveDashboard(DashboardViewModel dvm = null, List<string> imageData = null, FormCollection collection = null)
        //public ActionResult SaveDashboard(DashboardViewModel dvm, string budowa, string komenda, List<string> imageData, FormCollection collection)
        {
            if (dvm == null || collection == null)
            {
                try
                {
                    var json = JsonConvert.SerializeObject(dvm, Formatting.Indented);
                    var json2 = JsonConvert.SerializeObject(collection, Formatting.Indented);
                    var json3 = JsonConvert.SerializeObject(imageData, Formatting.Indented);
                    var filename = AppDomain.CurrentDomain.BaseDirectory + "Logi\\" + "Dashboard\\BlądDVM"  + DateTime.Now.ToString("dd-MM-yyyy") + ".txt";
                    var sw = new System.IO.StreamWriter(filename, true);
                    sw.WriteLine(DateTime.Now.ToString() + Environment.NewLine + json.ToString() + Environment.NewLine + "***************************************");
                    sw.Close();

                    Session["AdditionalMessage"] = "Coś poszło nie tak. Logi zapisane.";
                }
                catch (Exception e)
                {
                    Debug.WriteLine(e.Message);
                }
                return RedirectToAction("Index", "Home");
            }

            Budowa currentBudowa;
            string budowa = collection["budowa"].ToString();
            string komenda = collection["komenda"].ToString();
            string serwis = "";
            try
            {
                currentBudowa = budowy.FirstOrDefault(m => m.Id == Convert.ToInt32(budowa));
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
                currentBudowa = new Budowa(0, budowa, "0");
                serwis = currentBudowa.Nazwa;
            }

            //try
            //{
            dvm.Brygadzista = dvm.Brygada[0];
            dvm.Tydzien = (Tydzien)Session["Tydzien"]; //z sesji
            if (dvm.Tydzien == null) //z ciasteczka
            {
                dvm.Tydzien = Help.GetTydzien(Response.Cookies["Tydzien"].Value);
                Session["Tydzien"] = dvm.Tydzien;
            }
            if (dvm.Tydzien == null) //z kolekcji
            {
                dvm.Tydzien = Help.GetTydzien(Convert.ToInt32(collection["tydzien"]));
                Session["Tydzien"] = dvm.Tydzien;
                Response.Cookies.Add(new HttpCookie("Tydzien", dvm.Tydzien.getRokTydzien()));
            }
            //}
            //catch (Exception e)
            //{
            //    Debug.WriteLine(e.Message);

            //}

            string message = "";
            string colorMessage = "black";

            ViewBag.WybranaBudowa = currentBudowa;
            ViewBag.Budowy = budowy;

            //Sessions
            Session["Tydzien"] = dvm.Tydzien;
            Session["DVM"] = dvm;
            Session["Budowa"] = currentBudowa;

            if (komenda == "0" || komenda == "" || komenda == "2")
            {
                //Zapis do bazy
                //try
                //{
                pobiNTRANETDataContext dc = new pobiNTRANETDataContext();
                Help.SaveWeekToDatabase(dvm, currentBudowa, Server.MapPath("~/Content/signatures"), imageData, Server.MapPath("~/Logi/Dashboard/"), dvm.Tydzien);
                message = string.Format("Zapisano zmiany ({0})", DateTime.Now);
                colorMessage = "#00872B";
                //}
                //catch (Exception blad003)
                //{
                //    message = string.Format("Coś poszło nie tak. {0}{1}{2}", blad003.Message, Environment.NewLine, JsonConvert.SerializeObject(dvm, Formatting.Indented).ToString());
                //    colorMessage = "#FF3232";
                //}

                ViewBag.Message = message;
                ViewBag.ColorMessage = colorMessage;
                ViewBag.Podpisy = Help.GetPodpisy(dvm, currentBudowa);
                ViewBag.Serwis = serwis;

                //Sessions
                Session["Tydzien"] = dvm.Tydzien;
                Session["DVM"] = dvm;
                Session["Budowa"] = currentBudowa;

                return View("Index", dvm);
            }
            else
            {
                message = string.Format("zmieniono_budowe");
                colorMessage = "#4286f4";
            }

            dvm.DniTygodnia = Help.GetDniTygodnia(dvm.Brygada.Count, dvm.Tydzien, dvm.Brygada[0].Id, dvm.Brygada, currentBudowa.Id);
            ViewBag.WybranaBudowa = currentBudowa;
            ViewBag.Budowy = budowy;
            ViewBag.Podpisy = Help.GetPodpisy(dvm, currentBudowa);

            //Sessions
            Session["Tydzien"] = dvm.Tydzien;
            Session["DVM"] = dvm;
            Session["Budowa"] = currentBudowa;

            return RedirectToAction("Index2", "Dashboard", new { message = message, colorMessage = colorMessage, serwis = serwis });
        }

        [HttpPost]
        public void ChangeWeek(int tydzien, int budowa, int brygadzista, string staryTydzien)
        {
            Session["Tydzien"] = Help.GetTydzien(tydzien);
            Response.Cookies.Add(new HttpCookie("Tydzien", Help.GetTydzien(tydzien).getRokTydzien()));
            try
            {
                pobiNTRANETDataContext dc = new pobiNTRANETDataContext();
                int idgodziny = dc.AspNetGodzinies.Where(asg => asg.brygadzistaID == brygadzista && asg.budowaID == budowa && asg.rokTydzien == staryTydzien).First().Id;

                Help.ZmienGodziny(staryTydzien, tydzien, budowa, brygadzista);
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
            }
        }
    }
}
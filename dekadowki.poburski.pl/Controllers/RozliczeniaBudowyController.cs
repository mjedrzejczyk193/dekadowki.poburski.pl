﻿using dekadowki.poburski.pl.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Web.Mvc;

namespace dekadowki.poburski.pl.Controllers
{
    [Authorize]
    public class RozliczeniaBudowyController : Controller
    {
        List<Budowa> budowy = Help.GetBudowy();
        List<Tydzien> weeklist = Help.GetTygodnieCalosc(DateTime.Now.Year);
        pobiNTRANETDataContext dc = new pobiNTRANETDataContext();

        // GET: RozliczeniaBudowy
        public ActionResult Index()
        {
            Session.Clear();
            int id_kierownika = Help.GetIdIntranetFromDb(User.Identity.Name.ToString());
            int rozliczenie_id = 0;
            Budowa wybranaBudowa = new Budowa(-1, "", "");
            Tydzien wybranyTydzien = Help.GetTydzien(Help.GetCurrentWeek());

            AspNetRozliczeniaBudow rozliczenie = new AspNetRozliczeniaBudow();
            if (!string.IsNullOrEmpty(Request["roz"]))
            {
                rozliczenie_id = Convert.ToInt32(Request["roz"]);
                rozliczenie = dc.AspNetRozliczeniaBudows.Where(rb => rb.Id == rozliczenie_id).FirstOrDefault();
            }
            else
            {
                if (!string.IsNullOrEmpty(Request["bud"]))
                {
                    try
                    {
                        int budowa_id = Convert.ToInt32(Request["bud"]);
                        wybranaBudowa = budowy.Where(b => b.Id == budowa_id).FirstOrDefault();
                    }
                    catch (Exception e)
                    {
                        Debug.WriteLine("Budowa: " + e.Message);
                    }
                }
                if (!string.IsNullOrEmpty(Request["tyd"]))
                {
                    try
                    {
                        int tydzien_id = Convert.ToInt32(Request["tyd"]);
                        wybranyTydzien = weeklist.Where(t => t.getId() == tydzien_id).First();
                    }
                    catch (Exception e)
                    {
                        Debug.WriteLine("Tydzien: " + e.Message);
                    }
                }
                try
                {
                    if (dc.AspNetRozliczeniaBudows.Where(rb => rb.brygadzista_id == id_kierownika && rb.budowa_id == wybranaBudowa.Id && rb.tydzien_id == wybranyTydzien.getId()).Any())
                    {
                        rozliczenie = dc.AspNetRozliczeniaBudows.Where(rb => rb.brygadzista_id == id_kierownika && rb.budowa_id == wybranaBudowa.Id && rb.tydzien_id == wybranyTydzien.getId()).FirstOrDefault();
                    }
                    else
                    {
                        SprobujSciagnacZDekadowek(wybranaBudowa, wybranyTydzien, id_kierownika, ref rozliczenie);
                    }
                }
                catch(Exception e)
                {
                    Debug.WriteLine(e.Message);
                }
            }

            ViewBag.Rozliczenie = rozliczenie;
            ViewBag.Kierownik = id_kierownika;
            ViewBag.Budowy = budowy;
            ViewBag.Tygodnie = weeklist;
            ViewBag.WybranaBudowa = wybranaBudowa;
            ViewBag.WybranyTydzien = wybranyTydzien;
            return View();
        }

        private void SprobujSciagnacZDekadowek(Budowa wybranaBudowa, Tydzien wybranyTydzien, int id_kierownika, ref AspNetRozliczeniaBudow rozliczenie)
        {
            var godziny = dc.AspNetGodzinies.Where(pn => pn.brygadzistaID == id_kierownika && pn.rokTydzien == wybranyTydzien.getRokTydzien() && pn.budowaID == wybranaBudowa.Id);
            if (godziny.Any())
            {
                rozliczenie.godzPob_pn = godziny.Select(g => Convert.ToDouble(g.do_pn) - Convert.ToDouble(g.od_pn) + Convert.ToDouble(g.dojazd_pn)).Sum().ToString();
                rozliczenie.godzPob_wt = godziny.Select(g => Convert.ToDouble(g.do_wt) - Convert.ToDouble(g.od_wt) + Convert.ToDouble(g.dojazd_wt)).Sum().ToString();
                rozliczenie.godzPob_sr = godziny.Select(g => Convert.ToDouble(g.do_sr) - Convert.ToDouble(g.od_sr) + Convert.ToDouble(g.dojazd_sr)).Sum().ToString();
                rozliczenie.godzPob_cz = godziny.Select(g => Convert.ToDouble(g.do_cz) - Convert.ToDouble(g.od_cz) + Convert.ToDouble(g.dojazd_cz)).Sum().ToString();
                rozliczenie.godzPob_pi = godziny.Select(g => Convert.ToDouble(g.do_pi) - Convert.ToDouble(g.od_pi) + Convert.ToDouble(g.dojazd_pi)).Sum().ToString();
                rozliczenie.godzPob_so = godziny.Select(g => Convert.ToDouble(g.do_so) - Convert.ToDouble(g.od_so) + Convert.ToDouble(g.dojazd_so)).Sum().ToString();
            }
        }

        [HttpPost]
        public ActionResult SaveRozliczenie(FormCollection collection)
        {
            var budowa = collection["budowa"];
            var tydzien = collection["tydzien"];
            var podpis = collection["podpis"];
            var brygadzista_id = Convert.ToInt32(collection["kierownik"]);
            string path = Server.MapPath("~/Content/signatures");

            string fileName = string.Format("{0}-{1}-{2}-{3}-{4}.png", "rozliczenie", brygadzista_id, tydzien, budowa, DateTime.Now.ToShortDateString());
            string fileNameWitPath = Path.Combine(path, fileName);
            string currentPodpis = null;
            if (!string.IsNullOrEmpty(podpis) && podpis != Help.pustyPodpis)
            {
                using (FileStream fs = new FileStream(fileNameWitPath, FileMode.Create))
                {
                    using (BinaryWriter bw = new BinaryWriter(fs))
                    {
                        byte[] data = Convert.FromBase64String(podpis);
                        bw.Write(data);
                        bw.Close();
                    }
                    fs.Close();
                }
                currentPodpis = fileName;
            }

            AspNetRozliczeniaBudow rozliczenie = new AspNetRozliczeniaBudow();
            rozliczenie.budowa_id = Convert.ToInt32(budowa);
            rozliczenie.tydzien_id = Convert.ToInt32(tydzien);
            rozliczenie.brygadzista_id = brygadzista_id;
            rozliczenie.podpis = currentPodpis;
            //Powierzchnia Poburski
            rozliczenie.powPob_pn = collection["powPob_pn"];
            rozliczenie.powPob_wt = collection["powPob_wt"];
            rozliczenie.powPob_sr = collection["powPob_sr"];
            rozliczenie.powPob_cz = collection["powPob_cz"];
            rozliczenie.powPob_pi = collection["powPob_pi"];
            rozliczenie.powPob_so = collection["powPob_so"];
            //Powierzchnia Extra
            rozliczenie.powExtra_nazwa = collection["powExtra_nazwa"];
            rozliczenie.powExtra_pn = collection["powExtra_pn"];
            rozliczenie.powExtra_wt = collection["powExtra_wt"];
            rozliczenie.powExtra_sr = collection["powExtra_sr"];
            rozliczenie.powExtra_cz = collection["powExtra_cz"];
            rozliczenie.powExtra_pi = collection["powExtra_pi"];
            rozliczenie.powExtra_so = collection["powExtra_so"];
            //Godziny Poburski
            rozliczenie.godzPob_pn = collection["godzPob_pn"];
            rozliczenie.godzPob_wt = collection["godzPob_wt"];
            rozliczenie.godzPob_sr = collection["godzPob_sr"];
            rozliczenie.godzPob_cz = collection["godzPob_cz"];
            rozliczenie.godzPob_pi = collection["godzPob_pi"];
            rozliczenie.godzPob_so = collection["godzPob_so"];
            //Godziny Extra1
            rozliczenie.godzExtra1_nazwa = collection["godzExtra1_nazwa"];
            rozliczenie.godzExtra1_pn = collection["godzExtra1_pn"];
            rozliczenie.godzExtra1_wt = collection["godzExtra1_wt"];
            rozliczenie.godzExtra1_sr = collection["godzExtra1_sr"];
            rozliczenie.godzExtra1_cz = collection["godzExtra1_cz"];
            rozliczenie.godzExtra1_pi = collection["godzExtra1_pi"];
            rozliczenie.godzExtra1_so = collection["godzExtra1_so"];
            //Godziny Extra2
            rozliczenie.godzExtra2_nazwa = collection["godzExtra2_nazwa"];
            rozliczenie.godzExtra2_pn = collection["godzExtra2_pn"];
            rozliczenie.godzExtra2_wt = collection["godzExtra2_wt"];
            rozliczenie.godzExtra2_sr = collection["godzExtra2_sr"];
            rozliczenie.godzExtra2_cz = collection["godzExtra2_cz"];
            rozliczenie.godzExtra2_pi = collection["godzExtra2_pi"];
            rozliczenie.godzExtra2_so = collection["godzExtra2_so"];
            //Godziny Extra3
            rozliczenie.godzExtra3_nazwa = collection["godzExtra3_nazwa"];
            rozliczenie.godzExtra3_pn = collection["godzExtra3_pn"];
            rozliczenie.godzExtra3_wt = collection["godzExtra3_wt"];
            rozliczenie.godzExtra3_sr = collection["godzExtra3_sr"];
            rozliczenie.godzExtra3_cz = collection["godzExtra3_cz"];
            rozliczenie.godzExtra3_pi = collection["godzExtra3_pi"];
            rozliczenie.godzExtra3_so = collection["godzExtra3_so"];
            //Godziny Extra4
            rozliczenie.godzExtra4_nazwa = collection["godzExtra4_nazwa"];
            rozliczenie.godzExtra4_pn = collection["godzExtra4_pn"];
            rozliczenie.godzExtra4_wt = collection["godzExtra4_wt"];
            rozliczenie.godzExtra4_sr = collection["godzExtra4_sr"];
            rozliczenie.godzExtra4_cz = collection["godzExtra4_cz"];
            rozliczenie.godzExtra4_pi = collection["godzExtra4_pi"];
            rozliczenie.godzExtra4_so = collection["godzExtra4_so"];
            //Godziny dźwigu
            rozliczenie.dzwig_pn = collection["dzwig_pn"];
            rozliczenie.dzwig_wt = collection["dzwig_wt"];
            rozliczenie.dzwig_sr = collection["dzwig_sr"];
            rozliczenie.dzwig_cz = collection["dzwig_cz"];
            rozliczenie.dzwig_pi = collection["dzwig_pi"];
            rozliczenie.dzwig_so = collection["dzwig_so"];
            //Godziny cat
            rozliczenie.cat_pn = collection["cat_pn"];
            rozliczenie.cat_wt = collection["cat_wt"];
            rozliczenie.cat_sr = collection["cat_sr"];
            rozliczenie.cat_cz = collection["cat_cz"];
            rozliczenie.cat_pi = collection["cat_pi"];
            rozliczenie.cat_so = collection["cat_so"];
            //Manitou
            rozliczenie.manitou_pn = collection["manitou_pn"];
            rozliczenie.manitou_wt = collection["manitou_wt"];
            rozliczenie.manitou_sr = collection["manitou_sr"];
            rozliczenie.manitou_cz = collection["manitou_cz"];
            rozliczenie.manitou_pi = collection["manitou_pi"];
            rozliczenie.manitou_so = collection["manitou_so"];
            //Inne
            rozliczenie.inne_pn = collection["inne_pn"];
            rozliczenie.inne_wt = collection["inne_wt"];
            rozliczenie.inne_sr = collection["inne_sr"];
            rozliczenie.inne_cz = collection["inne_cz"];
            rozliczenie.inne_pt = collection["inne_pi"];
            rozliczenie.inne_so = collection["inne_so"];
            //Inne jakie?
            rozliczenie.notatki_pn = collection["notatki_pn"];
            rozliczenie.notatki_wt = collection["notatki_wt"];
            rozliczenie.notatki_sr = collection["notatki_sr"];
            rozliczenie.notatki_cz = collection["notatki_cz"];
            rozliczenie.notatki_pi = collection["notatki_pi"];
            rozliczenie.notatki_so = collection["notatki_so"];

            if(!dc.AspNetRozliczeniaBudows.Where(rb => rb.budowa_id == Convert.ToInt32(budowa) && rb.tydzien_id == Convert.ToInt32(tydzien) && rb.brygadzista_id == brygadzista_id).Any())
            {
                dc.AspNetRozliczeniaBudows.InsertOnSubmit(rozliczenie);
                dc.SubmitChanges();

                rozliczenie = Help.PoprawRozliczenie(rozliczenie);
            }
            else
            {
                var update = dc.AspNetRozliczeniaBudows.Single(rb => rb.tydzien_id == Convert.ToInt32(tydzien) && rb.budowa_id == Convert.ToInt32(budowa) && rb.brygadzista_id == brygadzista_id);
                update.podpis = currentPodpis;

                update = Help.PoprawRozliczenie(update);

                dc.SubmitChanges();
                rozliczenie = update;
            }
            List<string> listaUserow = new List<string>();
            listaUserow.Add("klechowicz@poburski.pl");
            listaUserow.Add("mjedrzejczyk@wizualizacja.com");
            Help.StworzExcelIWyslij(rozliczenie, "RozliczenieBudowy", listaUserow, User.Identity.Name.ToString(), Server.MapPath("~/Content/"));

            return RedirectToAction("Index", "RozliczeniaBudowy", new { bud = budowa, tyd = tydzien });
        }
    }
}
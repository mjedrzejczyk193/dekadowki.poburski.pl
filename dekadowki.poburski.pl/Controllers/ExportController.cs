﻿using System.Linq;
using System.Web.Mvc;
using dekadowki.poburski.pl.Models;
using System.Collections.Generic;
using System;
using System.Diagnostics;

namespace dekadowki.poburski.pl.Controllers
{
    public class ExportController : Controller
    {
        pobiNTRANETDataContext dc = new pobiNTRANETDataContext();

        // GET: Export
        [Authorize]
        public ActionResult Index(int id, string tydzien, int budowa)
        {
            List<Brygadzista> listaBrygadzistow = Help.GetBrygadzistowDoAkceptacji();
            List<string> tygodnieDoAkceptacji = new List<string>();
            if (id != -1)
            {
                tygodnieDoAkceptacji = dc.AspNetGodzinies.Where(t => t.brygadzistaID == id && t.export == 0).Select(t => t.rokTydzien).Distinct().ToList();
            }
            else
            {
                if (listaBrygadzistow.Count != 0)
                {
                    tygodnieDoAkceptacji = dc.AspNetGodzinies.Where(t => t.brygadzistaID == listaBrygadzistow[0].GetId() && t.export == 0).Select(t => t.rokTydzien).Distinct().ToList();
                }
            }
            ExportViewModel model = new ExportViewModel(listaBrygadzistow, tygodnieDoAkceptacji);
            List<AspNetGodziny> godziny = new List<AspNetGodziny>();
            if (tydzien != "a")
            {
                if (id == -1) id = listaBrygadzistow[0].GetId();
                godziny = dc.AspNetGodzinies.Where(q => q.brygadzistaID == id && q.rokTydzien == tydzien && q.export == 0).ToList();
            }
            else
            {
                if (tygodnieDoAkceptacji.Count > 0)
                {
                    if (id == -1) id = listaBrygadzistow[0].GetId();
                    tydzien = tygodnieDoAkceptacji[0];
                    godziny = dc.AspNetGodzinies.Where(q => q.brygadzistaID == id && q.rokTydzien == tydzien && q.export == 0).ToList();
                }
                else
                {
                    ViewBag.Message = "Brak tygodnia do zaakceptowania. Proszę wprowadzić dane.";
                }
            }

            if(budowa != -1)
            {
                godziny = godziny.Where(g => g.budowaID == budowa).ToList();
            }
            ViewBag.Preview = godziny;
            ViewBag.Budowy = Help.GetBudowy();

            return View(model);
        }

        [Authorize]
        public ActionResult Success(string message, string success, string color, int budowa)
        {
            ViewBag.Message = message;
            ViewBag.Color = color;
            ViewBag.Success = success;
            var list = (List<AspNetGodziny>)Session["GodzinyZaakceptowane"];
            if (budowa != 0)
            {
                foreach (var godzina in list)
                {
                    godzina.budowaID = budowa;
                }
            }
            ViewBag.Godziny = list;

            return View();
        }


        [Authorize]
        [HttpPost]
        public ActionResult Export(int brygadzista, string tydzien, int budowa, int budowaFiltr)
        {
            var brygadzistaNazwa = Help.GetNameAndSurname(brygadzista);
            var godziny = dc.AspNetGodzinies.Where(q => q.brygadzistaID == brygadzista && q.rokTydzien == tydzien && q.export == 0).ToList();
            if(budowaFiltr != -1)
            {
                godziny = godziny.Where(g => g.budowaID == budowa).ToList();
            }
            List<AspNetGodziny> godzinyZaakceptowane = new List<AspNetGodziny>();
            var message = "";
            var color = "";
            var success = "";
            if (tydzien != null)
            {
                try
                {
                    foreach (var godz in godziny)
                    {
                        akord_godziny akord_godziny_pn = Help.ConvertAspNetGodzinyToAkordGodziny(godz, 1, budowa);
                        akord_godziny akord_godziny_wt = Help.ConvertAspNetGodzinyToAkordGodziny(godz, 2, budowa);
                        akord_godziny akord_godziny_sr = Help.ConvertAspNetGodzinyToAkordGodziny(godz, 3, budowa);
                        akord_godziny akord_godziny_cz = Help.ConvertAspNetGodzinyToAkordGodziny(godz, 4, budowa);
                        akord_godziny akord_godziny_pi = Help.ConvertAspNetGodzinyToAkordGodziny(godz, 5, budowa);
                        akord_godziny akord_godziny_so = Help.ConvertAspNetGodzinyToAkordGodziny(godz, 6, budowa);
                        akord_godziny akord_godziny_ni = Help.ConvertAspNetGodzinyToAkordGodziny(godz, 7, budowa);

                        List<akord_godziny> lista = new List<akord_godziny>();
                        if (!sprawdzCzyIstnieje(akord_godziny_pn))
                        {
                            lista.Add(akord_godziny_pn);
                        }
                        //if (akord_godziny_wt.godz_od != 0.00 && akord_godziny_wt.godz_do != 0.00) 
                        if (!sprawdzCzyIstnieje(akord_godziny_wt))
                        {
                            lista.Add(akord_godziny_wt);
                        }
                        //if (akord_godziny_sr.godz_od != 0.00 && akord_godziny_sr.godz_do != 0.00) 
                        if (!sprawdzCzyIstnieje(akord_godziny_sr))
                        {
                            lista.Add(akord_godziny_sr);
                        }
                        //if (akord_godziny_cz.godz_od != 0.00 && akord_godziny_cz.godz_do != 0.00) 
                        if (!sprawdzCzyIstnieje(akord_godziny_cz))
                        {
                            lista.Add(akord_godziny_cz);
                        }
                        //if (akord_godziny_pi.godz_od != 0.00 && akord_godziny_pi.godz_do != 0.00) 
                        if (!sprawdzCzyIstnieje(akord_godziny_pi))
                        {
                            lista.Add(akord_godziny_pi);
                        }
                        //if (akord_godziny_so.godz_od != 0.00 && akord_godziny_so.godz_do != 0.00) 
                        if (!sprawdzCzyIstnieje(akord_godziny_so))
                        {
                            lista.Add(akord_godziny_so);
                        }
                        //if (akord_godziny_ni.godz_od != 0.00 && akord_godziny_ni.godz_do != 0.00) 
                        if (!sprawdzCzyIstnieje(akord_godziny_ni))
                        {
                            lista.Add(akord_godziny_ni);
                        }

                        //Tworzenie rekordów
                        dc.akord_godzinies.InsertAllOnSubmit(lista);
                        dc.SubmitChanges();
                        //Zmiana na eksportowano
                        try
                        {
                            var godzinyDoZaakceptowania = dc.AspNetGodzinies.Where(g => g.budowaID == godz.budowaID && g.pracownikID == godz.pracownikID && g.rokTydzien == godz.rokTydzien && g.export == 0).ToList();
                            foreach (var godzina in godzinyDoZaakceptowania)
                            {
                                //var current = dc.AspNetGodzinies.First(g => g.budowaID == godz.budowaID && g.pracownikID == godz.pracownikID && g.rokTydzien == godz.rokTydzien && g.export == 0);
                                godzina.export = 1;
                                dc.SubmitChanges();
                            }
                        }
                        catch(Exception bladAkceptacji)
                        {
                            //Usuwanie tylko pierwszych jeśli błąd
                            var current = dc.AspNetGodzinies.First(g => g.budowaID == godz.budowaID && g.pracownikID == godz.pracownikID && g.rokTydzien == godz.rokTydzien && g.export == 0);
                            current.export = 1;
                            dc.SubmitChanges();
                            Debug.WriteLine(bladAkceptacji.Message);
                        }

                        godzinyZaakceptowane.Add(godz);
                    }
                    message = string.Format("Sukces. Pomyślnie eksportowano wszystkie godziny brygadzisty {0} z tygodnia {1} do intranetu.", brygadzistaNazwa, tydzien);
                    success = "1";
                    color = "green";
                }
                catch (Exception e)
                {
                    message = string.Format("Pojawił się błąd. Brygadzista: {0}, Tydzień: {1} Skontaktuj się z administratorem. Szczegóły błędu: {2}", brygadzistaNazwa, tydzien, e.Message);
                    success = "0";
                    color = "red";
                }
                Session["GodzinyZaakceptowane"] = godzinyZaakceptowane;
            }

            return RedirectToAction("Success", "Export", new { message = message, success = success, color = color, budowa = budowa });
        }

        private bool sprawdzCzyIstnieje(akord_godziny akord_godziny)
        {
            if (!dc.akord_godzinies.Any(g => g.Data == akord_godziny.Data && g.idbudowy == akord_godziny.idbudowy && g.id_prac == akord_godziny.id_prac))
            {
                return false;
            }
            else if (dc.akord_godzinies.FirstOrDefault(g => g.Data == akord_godziny.Data && g.idbudowy == akord_godziny.idbudowy && g.id_prac == akord_godziny.id_prac).godz_akord <= 0)
            {
                dc.akord_godzinies.DeleteOnSubmit(dc.akord_godzinies.FirstOrDefault(g => g.Data == akord_godziny.Data && g.idbudowy == akord_godziny.idbudowy && g.id_prac == akord_godziny.id_prac));
                dc.SubmitChanges();
                return false;
            }
            return true;
        }
    }
}
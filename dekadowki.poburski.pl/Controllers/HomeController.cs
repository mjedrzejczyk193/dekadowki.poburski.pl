﻿using dekadowki.poburski.pl.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace dekadowki.poburski.pl.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            Help.generateAdminMessages();

            List<string> errorList = new List<string>();
            List<Brygada> brygada = new List<Brygada>();
            List<Tydzien> weeklist = new List<Tydzien>();
            List<Brygadzista> brygadzisci = new List<Brygadzista>();
            List<AspNetMessage> wiadomosci = new List<AspNetMessage>();
            try
            {
                int id_kierownika = Help.GetIdIntranetFromDb(User.Identity.Name.ToString());
                brygada = Help.GetBrygada(id_kierownika, false);

                wiadomosci = Help.GetAdminMessages(id_kierownika);
            }
            catch (Exception blad001)
            {
                errorList.Add(string.Format("Nie można odtworzyć brygady użytkownika {0}. {1}", User.Identity.Name, blad001.Message));
            }

            try
            {
                brygadzisci = Help.GetBrygadzistow(true);
            }
            catch (Exception blad002)
            {
                errorList.Add(string.Format("Nie można pobrać listy brygadzistów. Brak możliwości dodania pracowników. {0}", blad002.Message));
            }
            Help.BlockWeekIfNotNeeded();
            weeklist = Help.GetTygodnie(DateTime.Now.Year);
            var additionalMessages = Session["AdditionalMessage"] != null ? Session["AdditionalMessage"].ToString() : "";
            if (!string.IsNullOrEmpty(additionalMessages))
            {
                errorList.Add(additionalMessages);

                Session["AdditionalMessage"] = null;
            }

            ViewBag.ErrorList = errorList;
            ViewBag.Brygada = brygada;
            ViewBag.Brygadzisci = brygadzisci;
            ViewBag.Tydzien = weeklist;
            ViewBag.Wiadomosci = wiadomosci;

            //Dodawanie i usuwanie
            if (Help.isSession(Session["NowaBrygada"]))
            {
                ViewBag.Brygada = (List<Brygada>)Session["NowaBrygada"];
            }
            else
            {
                Session["NowaBrygada"] = brygada;
            }

            Session["Tydzien"] = weeklist[0];

            return View();
        }

        public ActionResult Preview()
        {
            Session.Clear();
            ViewBag.Preview = GetGodzinyDoPogladu100();
            return View();
        }

        [HttpPost]
        public ActionResult FiltrujPodglad(int budowa, int brygadzista, int tydzien)
        {
            ViewBag.Preview = GetGodzinyDoPogladu(budowa, brygadzista, tydzien);
            Session["FiltrBudowa"] = budowa;
            Session["FiltrBrygadzista"] = brygadzista;
            Session["FiltrTydzien"] = tydzien;
            return View("Preview");
        }

        private List<AspNetGodziny> GetGodzinyDoPogladu()
        {
            pobiNTRANETDataContext dc = new pobiNTRANETDataContext();
            int id_kierownika = Help.GetIdIntranetFromDb(User.Identity.Name.ToString());
            var godziny = dc.AspNetGodzinies.Where(q => q.brygadzistaID == id_kierownika).ToList();
            if (Help.SprawdzCzyAdmin(User.Identity.Name.ToString()))
            {
                godziny = dc.AspNetGodzinies.ToList();
            }

            return godziny.OrderByDescending(q => q.rokTydzien).ThenBy(q => q.brygadzistaID).ThenBy(q => q.budowaID).ToList();
        }

        private List<AspNetGodziny> GetGodzinyDoPogladu100()
        {
            pobiNTRANETDataContext dc = new pobiNTRANETDataContext();
            int id_kierownika = Help.GetIdIntranetFromDb(User.Identity.Name.ToString());
            var godziny = dc.AspNetGodzinies.Where(q => q.brygadzistaID == id_kierownika).OrderByDescending(q => q.rokTydzien).ThenBy(q => q.brygadzistaID).ThenBy(q => q.budowaID).Take(100).ToList();
            if (Help.SprawdzCzyAdmin(User.Identity.Name.ToString()))
            {
                godziny = dc.AspNetGodzinies.Take(100).OrderByDescending(q => q.rokTydzien).ThenBy(q => q.brygadzistaID).ThenBy(q => q.budowaID).ToList();
            }

            return godziny; //.OrderByDescending(q => q.rokTydzien).ThenBy(q => q.brygadzistaID).ThenBy(q => q.budowaID).ToList();
        }

        private List<AspNetGodziny> GetGodzinyDoPogladu(int budowa_filtr, int brygadzista_filtr, int tydzien_filtr)
        {
            pobiNTRANETDataContext dc = new pobiNTRANETDataContext();
            int id_kierownika = Help.GetIdIntranetFromDb(User.Identity.Name.ToString());
            var godziny = dc.AspNetGodzinies.Where(q => q.brygadzistaID == id_kierownika).ToList();

            if (Help.SprawdzCzyAdmin(User.Identity.Name.ToString()))
            {
                godziny = dc.AspNetGodzinies.ToList();
            }

            if (budowa_filtr != 0)
            {
                godziny = godziny.Where(g => g.budowaID == budowa_filtr).ToList();
            }

            if (brygadzista_filtr != 0)
            {
                godziny = godziny.Where(g => g.brygadzistaID == brygadzista_filtr).ToList();
            }

            if (tydzien_filtr != 0)
            {
                godziny = godziny.Where(g => g.rokTydzien == Help.GetTydzien(tydzien_filtr).getRokTydzien()).ToList();
            }

            return godziny.OrderByDescending(q => q.rokTydzien).ThenBy(q => q.brygadzistaID).ThenBy(q => q.budowaID).ToList();
        }

        public ActionResult DeleteGodziny(int id)
        {
            pobiNTRANETDataContext dc = new pobiNTRANETDataContext();
            AspNetGodziny aspnetgodziny = dc.AspNetGodzinies.First(g => g.Id == id);
            dc.AspNetGodzinies.DeleteOnSubmit(aspnetgodziny);
            dc.SubmitChanges();
            return RedirectToAction("Preview", "Home");
        }

        public ActionResult EksportGodziny(int id)
        {
            pobiNTRANETDataContext dc = new pobiNTRANETDataContext();
            AspNetGodziny aspnetgodziny = dc.AspNetGodzinies.First(g => g.Id == id);
            aspnetgodziny.export = aspnetgodziny.export == 0 ? aspnetgodziny.export = 1 : aspnetgodziny.export = 0;
            dc.SubmitChanges();
            Session["Zmiana"] = "Eksport";
            Session["ZmienioneID"] = id;
            return RedirectToAction("Preview", "Home");
        }

        public ActionResult EditGodziny(int id, bool? print)
        {
            pobiNTRANETDataContext dc = new pobiNTRANETDataContext();
            AspNetGodziny g = dc.AspNetGodzinies.First(godz => godz.Id == id);

            DashboardViewModel dvm = new DashboardViewModel();
            dvm.Tydzien = Help.GetTydzien(g.rokTydzien);
            var brygadzista = dc.pracowniks.FirstOrDefault(p => p.idpracownika == g.pracownikID);
            dvm.Brygadzista = new Brygada(Help.GetNameAndSurname(brygadzista.idpracownika), brygadzista.idpracownika, "", 0);
            var currentBudowa = Help.GetBudowa(g.budowaID);
            dvm.Brygada = Help.GetBrygadaZGodzin(g.brygadzistaID, currentBudowa, dvm.Tydzien);
            dvm.DniTygodnia = Help.GetDniTygodnia(dvm.Brygada.Count, dvm.Tydzien, dvm.Brygada[0].Id, dvm.Brygada, currentBudowa.Id);

            ViewBag.WybranaBudowa = currentBudowa;
            ViewBag.Budowy = Help.GetBudowy();
            ViewBag.Podpisy = Help.GetPodpisy(dvm, currentBudowa);

            //Sessions
            Session["Tydzien"] = dvm.Tydzien;
            Response.Cookies.Add(new HttpCookie(dvm.Tydzien.getRokTydzien()));
            Session["DVM"] = dvm;
            Session["Budowa"] = currentBudowa;
            Session["NowaBrygada"] = dvm.Brygada;
            var ms = "";
            var cms = "";
            var s = "";

            return RedirectToAction("Index2", "Dashboard", new { message = ms, colorMessage = cms, serwis = s, drukuj = print });
        }

        //public ActionResult PrintGodziny(int id)
        //{
        //    pobiNTRANETDataContext dc = new pobiNTRANETDataContext();
        //    AspNetGodziny g = dc.AspNetGodzinies.First(godz => godz.Id == id);

        //    DashboardViewModel dvm = new DashboardViewModel();
        //    dvm.Tydzien = Help.GetTydzien(g.rokTydzien);
        //    var brygadzista = dc.pracowniks.FirstOrDefault(p => p.idpracownika == g.pracownikID);
        //    dvm.Brygadzista = new Brygada(Help.GetNameAndSurname(brygadzista.idpracownika), brygadzista.idpracownika, Help.GetNameAndSurname(brygadzista.idpracownika), brygadzista.idpracownika);
        //    var currentBudowa = Help.GetBudowa(g.budowaID);
        //    dvm.Brygada = Help.GetBrygadaZGodzin(currentBudowa, dvm.Tydzien);
        //    dvm.DniTygodnia = Help.GetDniTygodnia(dvm.Brygada.Count, dvm.Tydzien, dvm.Brygada[0].Id, dvm.Brygada, currentBudowa.Id);

        //    //Sessions
        //    Session["Tydzien"] = dvm.Tydzien;
        //    Session["DVM"] = dvm;
        //    Session["Budowa"] = currentBudowa;

        //    return RedirectToAction("Print", "Dashboard");
        //}

        public ActionResult DeleteGodzinyNew(int id)
        {
            pobiNTRANETDataContext dc = new pobiNTRANETDataContext();
            AspNetGodziny aspnetgodziny = dc.AspNetGodzinies.First(g => g.Id == id);
            dc.AspNetGodzinies.DeleteOnSubmit(aspnetgodziny);
            dc.SubmitChanges();
            return RedirectToAction("PreviewNew", "Home");
        }

        public ActionResult EksportGodzinyNew(int id)
        {
            pobiNTRANETDataContext dc = new pobiNTRANETDataContext();
            AspNetGodziny aspnetgodziny = dc.AspNetGodzinies.First(g => g.Id == id);
            aspnetgodziny.export = aspnetgodziny.export == 0 ? aspnetgodziny.export = 1 : aspnetgodziny.export = 0;
            dc.SubmitChanges();
            return RedirectToAction("PreviewNew", "Home");
        }

        public ActionResult Blockade(int year)
        {
            ViewBag.Lata = Help.GetLata();
            return View(Help.GetTygodnieZablokowaneLubNie(year));
        }

        public ActionResult Lock(int year, int id)
        {
            pobiNTRANETDataContext dc = new pobiNTRANETDataContext();
            AspNetLata tydzien = dc.AspNetLatas.First(l => l.idtygodnia == id);
            tydzien.zablokowany = 1;
            dc.SubmitChanges();
            return RedirectToAction("Blockade", "Home", new { year = year });
        }

        public ActionResult Unlock(int year, int id)
        {
            pobiNTRANETDataContext dc = new pobiNTRANETDataContext();
            AspNetLata tydzien = dc.AspNetLatas.First(l => l.idtygodnia == id);
            tydzien.zablokowany = 2;
            dc.SubmitChanges();
            return RedirectToAction("Blockade", "Home", new { year = year });
        }

        [HttpPost]
        public ActionResult Add(int? answer)
        {
            try
            {
                int index = Convert.ToInt32(answer);
                if (index == 0)
                {
                    Session["AdditionalMessage"] += string.Format("Pracownik nie istieje w bazie. Nie można wpisać pracownika z palca, trzeba go wybrać. Skontaktuj się z działem personalnym aby dodał tego pracownika do Twojej brygady.");
                }
                else
                {
                    var brygada = Session["NowaBrygada"] != null ? (List<Brygada>)Session["NowaBrygada"] : Help.GetBrygada(Help.GetBrygadzistaID(index), true);
                    brygada.Add(new Brygada(Help.GetNameAndSurname(index), index, Help.GetBrygadzistaNazwa(index), Help.GetBrygadzistaID(index)));
                    Session["NowaBrygada"] = brygada.OrderBy(b => b.Stanowisko).ThenBy(b => b.getNazwa()).ToList();
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
                Session["AdditionalMessage"] += string.Format("Pracownik nie istieje w bazie. Skontaktuj się z działem personalnym aby dodał tego pracownika do Twojej brygady.");
            }

            return RedirectToAction("Index");
        }

        public ActionResult Delete(int idpracownika)
        {
            try
            {
                var brygada = Session["NowaBrygada"] != null ? (List<Brygada>)Session["NowaBrygada"] : Help.GetBrygada(Help.GetBrygadzistaID(idpracownika), true);
                brygada = Help.NowaBrygadaDelete(idpracownika, brygada);
                Session["NowaBrygada"] = brygada.OrderBy(b => b.Stanowisko).ThenBy(b => b.getNazwa()).ToList();
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
            }
            return RedirectToAction("Index");
        }

        public ActionResult DeleteAll()
        {
            var brygada = Session["NowaBrygada"] != null ? (List<Brygada>)Session["NowaBrygada"] : null;
            if (brygada != null)
            {
                var brygadaLista = new List<Brygada>();
                brygadaLista.Add(brygada[0]);
                Session["NowaBrygada"] = brygadaLista;
            }
            return RedirectToAction("Index");
        }

        public ActionResult GoBack()
        {
            var brygada = Session["NowaBrygada"] != null ? (List<Brygada>)Session["NowaBrygada"] : null;
            if (brygada != null)
            {
                Session["NowaBrygada"] = Help.GetBrygada(brygada[0].Id, false);
            }
            return RedirectToAction("Index");
        }

        [HttpPost]
        public void ChangeWeek(int tydzien)
        {
            Session["Tydzien"] = Help.GetTydzien(tydzien);
            Response.Cookies.Add(new HttpCookie("Tydzien", Help.GetTydzien(tydzien).getRokTydzien()));
        }

        [HttpPost]
        public void ReadAllMessages(int brygadzistaid)
        {
            pobiNTRANETDataContext dc = new pobiNTRANETDataContext();
            dc.AspNetMessages.Where(m => m.id_pracownika == brygadzistaid && m.data < DateTime.Now).ToList().ForEach(p => p.przeczytane = true);
            dc.SubmitChanges();
        }

        [OutputCacheAttribute(VaryByParam = "*", Duration = 0, NoStore = true)]
        public ActionResult Filtrowanie()
        {
            return View();
        }
    }
}
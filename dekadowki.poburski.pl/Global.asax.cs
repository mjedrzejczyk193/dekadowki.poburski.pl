﻿using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Configuration;
using System;
using System.Web;
using System.Net.Mail;
using System.Diagnostics;

namespace dekadowki.poburski.pl
{
    public class MvcApplication : System.Web.HttpApplication
    {
        private static double TimerIntervalInMilliseconds = Convert.ToDouble(WebConfigurationManager.AppSettings["TimerIntervalInMilliseconds"]);

        protected void Application_Start()
        {
            GlobalFilters.Filters.Add(new RequireHttpsAttribute());

            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }

        protected void Session_Start()
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {
            string To = "mjedrzejczyk@wizualizacja.com";
            string From = "mjedrzejczyk@wizualizacja.com";
            try
            {
                var httpContext = ((MvcApplication)sender).Context;
                var currentController = " ";
                var currentAction = " ";
                var currentRouteData = RouteTable.Routes.GetRouteData(new HttpContextWrapper(httpContext));

                if (currentRouteData != null)
                {
                    if (currentRouteData.Values["controller"] != null && !String.IsNullOrEmpty(currentRouteData.Values["controller"].ToString()))
                    {
                        currentController = currentRouteData.Values["controller"].ToString();
                    }

                    if (currentRouteData.Values["action"] != null && !String.IsNullOrEmpty(currentRouteData.Values["action"].ToString()))
                    {
                        currentAction = currentRouteData.Values["action"].ToString();
                    }
                }

                var ex = Server.GetLastError();
                //var controller = new ErrorController();
                var routeData = new RouteData();
                var action = "Błąd wygenerowany";

                if (ex is HttpException)
                {
                    var httpEx = ex as HttpException;

                    switch (httpEx.GetHttpCode())
                    {
                        case 404:
                            action = "404 - NotFound";
                            break;
                        default:
                            action = httpEx.GetHttpCode().ToString();
                            break;
                    }
                }

                // Get stack trace for the exception with source file information
                var st = new StackTrace(ex, true);
                // Get the top stack frame
                var frame = st.GetFrame(0);
                // Get the line number from the stack frame
                var line = frame.GetFileLineNumber();

                var user = HttpContext.Current.User.Identity;

                var ip = GetIp();

                string Subject = "Dekadowki - Błąd aplikacji " + user.Name;
                string Body = string.Format("<html><body><p>Kontroler: {0}</p><p>Akcja: {1}</p><p>Błąd: {2}</p><p>Header: {3}</p><p>Użytkownik: {4}</p><p>Linia: {5}</p><p>Ip: {6}</p></body><html>", currentController, currentAction, ex.Message, action, user.Name, line, ip);

                MailMessage msg = new MailMessage(From, To, Subject, Body); // create the email message
                msg.IsBodyHtml = true;

                SmtpClient client = new SmtpClient();
                client.UseDefaultCredentials = false;
                client.Credentials = new System.Net.NetworkCredential("mjedrzejczyk@wizualizacja.com", "WizMJ2018");
                client.Port = 587; // You can use Port 25 if 587 is blocked (mine is!)
                client.Host = "smtp.office365.com";
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.EnableSsl = true;

                client.Send(msg);
                Debug.WriteLine(string.Format("Wiadomość została wysłana do {0}", To));
                client.Dispose();
                msg.Dispose();
            }
            catch (Exception ex1)
            {
                Debug.WriteLine(ex1.Message);
                Debug.WriteLine(string.Format("Wiadomość NIE została wysłana do {0}", To));
            }

            
        }

        private string GetIp()
        {
            string ip = System.Web.HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
            if (string.IsNullOrEmpty(ip))
            {
                ip = System.Web.HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
            }
            return ip;
        }
    }
}

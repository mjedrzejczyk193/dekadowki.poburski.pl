﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace dekadowki.poburski.pl.Models
{
    public class Obecnosc
    {
        public string Id { get; set; }
        public string Nazwa { get; set; }
        public bool Wybrany { get; set; }

        public Obecnosc(string id, string nazwa, bool wybrany)
        {
            this.Id = id;
            this.Nazwa = nazwa;
            this.Wybrany = wybrany;
        }
    }
}
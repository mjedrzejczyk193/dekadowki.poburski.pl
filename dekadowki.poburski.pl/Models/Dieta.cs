﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace dekadowki.poburski.pl.Models
{
    public class Dieta
    {
        public int Id { get; set; }
        public string Nazwa { get; set; }

        public Dieta(int id, string nazwa)
        {
            this.Id = id;
            this.Nazwa = nazwa;
        }
    }
}
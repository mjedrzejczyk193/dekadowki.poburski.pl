﻿namespace dekadowki.poburski.pl.Models
{
    public class Brygada
    {
        public string Nazwa { get; set; }
        public int Id { get; set; }
        public string Brygadzista { get; set; }
        public int BrygadzistaId { get; set; }
        public int Stanowisko { get; set; }

        public Brygada()
        {

        }

        public Brygada(string nazwa, int id, string brygadzista, int brygadzistaId)
        {
            this.Nazwa = nazwa;
            this.Id = id;
            this.Brygadzista = brygadzista;
            this.BrygadzistaId = brygadzistaId;
            this.Stanowisko = Help.GetStanowisko(id);
        }

        public int getId()
        {
            return this.Id;
        }

        public int getBrygadzistaId()
        {
            return this.BrygadzistaId;
        }

        public string getNazwa()
        {
            return this.Nazwa;
        }

        public string getBrygadzistaNazwa()
        {
            return this.Brygadzista;
        }
    }
}
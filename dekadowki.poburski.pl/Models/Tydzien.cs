﻿namespace dekadowki.poburski.pl.Models
{
    public class Tydzien
    {
        int id { get; set; }
        string rokTydzien { get; set; }
        int zablokowany { get; set; }
        int rok { get; set; }

        public Tydzien(int id, string rokTydzien, int zablokowany, int rok)
        {
            this.id = id;
            this.rokTydzien = rokTydzien;
            this.zablokowany = zablokowany;
            this.rok = rok;
        }

        public int getId()
        {
            return this.id;
        }

        public string getRokTydzien()
        {
            return this.rokTydzien;
        }

        public int getZablokowany()
        {
            return this.zablokowany;
        }

        public int getRok()
        {
            return this.rok;
        }
    }
}
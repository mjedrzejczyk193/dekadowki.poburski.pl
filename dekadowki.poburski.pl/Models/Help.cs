﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Web;

namespace dekadowki.poburski.pl.Models
{

    public static class Help
    {
        static string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
        public static string pustyPodpis = "iVBORw0KGgoAAAANSUhEUgAAASwAAACWCAYAAABkW7XSAAAEYklEQVR4Xu3UAQkAAAwCwdm/9HI83BLIOdw5AgQIRAQWySkmAQIEzmB5AgIEMgIGK1OVoAQIGCw/QIBARsBgZaoSlAABg+UHCBDICBisTFWCEiBgsPwAAQIZAYOVqUpQAgQMlh8gQCAjYLAyVQlKgIDB8gMECGQEDFamKkEJEDBYfoAAgYyAwcpUJSgBAgbLDxAgkBEwWJmqBCVAwGD5AQIEMgIGK1OVoAQIGCw/QIBARsBgZaoSlAABg+UHCBDICBisTFWCEiBgsPwAAQIZAYOVqUpQAgQMlh8gQCAjYLAyVQlKgIDB8gMECGQEDFamKkEJEDBYfoAAgYyAwcpUJSgBAgbLDxAgkBEwWJmqBCVAwGD5AQIEMgIGK1OVoAQIGCw/QIBARsBgZaoSlAABg+UHCBDICBisTFWCEiBgsPwAAQIZAYOVqUpQAgQMlh8gQCAjYLAyVQlKgIDB8gMECGQEDFamKkEJEDBYfoAAgYyAwcpUJSgBAgbLDxAgkBEwWJmqBCVAwGD5AQIEMgIGK1OVoAQIGCw/QIBARsBgZaoSlAABg+UHCBDICBisTFWCEiBgsPwAAQIZAYOVqUpQAgQMlh8gQCAjYLAyVQlKgIDB8gMECGQEDFamKkEJEDBYfoAAgYyAwcpUJSgBAgbLDxAgkBEwWJmqBCVAwGD5AQIEMgIGK1OVoAQIGCw/QIBARsBgZaoSlAABg+UHCBDICBisTFWCEiBgsPwAAQIZAYOVqUpQAgQMlh8gQCAjYLAyVQlKgIDB8gMECGQEDFamKkEJEDBYfoAAgYyAwcpUJSgBAgbLDxAgkBEwWJmqBCVAwGD5AQIEMgIGK1OVoAQIGCw/QIBARsBgZaoSlAABg+UHCBDICBisTFWCEiBgsPwAAQIZAYOVqUpQAgQMlh8gQCAjYLAyVQlKgIDB8gMECGQEDFamKkEJEDBYfoAAgYyAwcpUJSgBAgbLDxAgkBEwWJmqBCVAwGD5AQIEMgIGK1OVoAQIGCw/QIBARsBgZaoSlAABg+UHCBDICBisTFWCEiBgsPwAAQIZAYOVqUpQAgQMlh8gQCAjYLAyVQlKgIDB8gMECGQEDFamKkEJEDBYfoAAgYyAwcpUJSgBAgbLDxAgkBEwWJmqBCVAwGD5AQIEMgIGK1OVoAQIGCw/QIBARsBgZaoSlAABg+UHCBDICBisTFWCEiBgsPwAAQIZAYOVqUpQAgQMlh8gQCAjYLAyVQlKgIDB8gMECGQEDFamKkEJEDBYfoAAgYyAwcpUJSgBAgbLDxAgkBEwWJmqBCVAwGD5AQIEMgIGK1OVoAQIGCw/QIBARsBgZaoSlAABg+UHCBDICBisTFWCEiBgsPwAAQIZAYOVqUpQAgQMlh8gQCAjYLAyVQlKgIDB8gMECGQEDFamKkEJEDBYfoAAgYyAwcpUJSgBAgbLDxAgkBEwWJmqBCVAwGD5AQIEMgIGK1OVoAQIGCw/QIBARsBgZaoSlACBB1YxAJfjJb2jAAAAAElFTkSuQmCC";

        public static List<AspNetMessage> GetAdminMessages(int id_kierownika)
        {
            pobiNTRANETDataContext dc = new pobiNTRANETDataContext();
            List<AspNetMessage> wiadomosci = dc.AspNetMessages.Where(l => l.id_pracownika == id_kierownika && l.przeczytane == false && l.data < DateTime.Now).ToList();

            return wiadomosci;
        }

        public static void generateAdminMessages()
        {
            DateTime date = DateTime.Now;
            var firstDayOfMonth = new DateTime(date.Year, date.Month, 1);
            var lastDayOfMonth = firstDayOfMonth.AddMonths(1).AddDays(-1);
            pobiNTRANETDataContext dc = new pobiNTRANETDataContext();
            List<Brygadzista> listaBrygadzistow = GetBrygadzistow(true);
            List<AspNetMessage> listaWiadomosci = new List<AspNetMessage>();
            foreach (var brygadzista in listaBrygadzistow)
            {
                AspNetMessage message = new AspNetMessage();
                message.tresc = "Proszę przesłać przebieg samochodu.";
                message.przeczytane = false;
                message.data = firstDayOfMonth;
                message.id_pracownika = brygadzista.GetId();
                if (!dc.AspNetMessages.Any(m => m.id_pracownika == message.id_pracownika && m.data == firstDayOfMonth))
                {
                    listaWiadomosci.Add(message);
                }
                else
                {
                    break;
                }
            }
            foreach (var brygadzista in listaBrygadzistow)
            {
                AspNetMessage message = new AspNetMessage();
                message.tresc = "Proszę przesłać przebieg samochodu.";
                message.przeczytane = false;
                message.data = lastDayOfMonth;
                message.id_pracownika = brygadzista.GetId();
                if (!dc.AspNetMessages.Any(m => m.id_pracownika == message.id_pracownika && m.data == lastDayOfMonth))
                {
                    listaWiadomosci.Add(message);
                }
                else
                {
                    break;
                }
            }

            dc.AspNetMessages.InsertAllOnSubmit(listaWiadomosci);
            dc.SubmitChanges();

            DeleteNotReadedAndExpired(dc);
        }

        private static void DeleteNotReadedAndExpired(pobiNTRANETDataContext dc)
        {
            var currentMonth = DateTime.Now.Month;
            var currentYear = DateTime.Now.Year;
            List<AspNetMessage> notAcceptedMessages = dc.AspNetMessages.Where(m => m.przeczytane == false).ToList();
            foreach (var nam in notAcceptedMessages)
            {
                bool delete = false;
                var date = DateTime.Parse(nam.data.ToString());
                if ((date.Year == currentYear && currentMonth - date.Month > 1) || date.Year < currentYear)
                {
                    delete = true;
                }
                if (delete)
                {
                    dc.AspNetMessages.DeleteOnSubmit(nam);
                    dc.SubmitChanges();
                }
            }
        }

        public static AspNetGodziny PoprawGodziny(AspNetGodziny godziny)
        {
            godziny.od_pn = string.IsNullOrEmpty(godziny.od_pn) ? null : godziny.od_pn.Replace(" ", "");
            godziny.od_wt = string.IsNullOrEmpty(godziny.od_wt) ? null : godziny.od_wt.Replace(" ", "");
            godziny.od_sr = string.IsNullOrEmpty(godziny.od_sr) ? null : godziny.od_sr.Replace(" ", "");
            godziny.od_cz = string.IsNullOrEmpty(godziny.od_cz) ? null : godziny.od_cz.Replace(" ", "");
            godziny.od_pi = string.IsNullOrEmpty(godziny.od_pi) ? null : godziny.od_pi.Replace(" ", "");
            godziny.od_so = string.IsNullOrEmpty(godziny.od_so) ? null : godziny.od_so.Replace(" ", "");
            godziny.od_ni = string.IsNullOrEmpty(godziny.od_ni) ? null : godziny.od_ni.Replace(" ", "");
            godziny.do_pn = string.IsNullOrEmpty(godziny.do_pn) ? null : godziny.do_pn.Replace(" ", "");
            godziny.do_wt = string.IsNullOrEmpty(godziny.do_wt) ? null : godziny.do_wt.Replace(" ", "");
            godziny.do_sr = string.IsNullOrEmpty(godziny.do_sr) ? null : godziny.do_sr.Replace(" ", "");
            godziny.do_cz = string.IsNullOrEmpty(godziny.do_cz) ? null : godziny.do_cz.Replace(" ", "");
            godziny.do_pi = string.IsNullOrEmpty(godziny.do_pi) ? null : godziny.do_pi.Replace(" ", "");
            godziny.do_so = string.IsNullOrEmpty(godziny.do_so) ? null : godziny.do_so.Replace(" ", "");
            godziny.do_ni = string.IsNullOrEmpty(godziny.do_ni) ? null : godziny.do_ni.Replace(" ", "");
            godziny.przestoje_pn = string.IsNullOrEmpty(godziny.przestoje_pn) ? null : godziny.przestoje_pn.Replace(" ", "");
            godziny.przestoje_wt = string.IsNullOrEmpty(godziny.przestoje_wt) ? null : godziny.przestoje_wt.Replace(" ", "");
            godziny.przestoje_sr = string.IsNullOrEmpty(godziny.przestoje_sr) ? null : godziny.przestoje_sr.Replace(" ", "");
            godziny.przestoje_cz = string.IsNullOrEmpty(godziny.przestoje_cz) ? null : godziny.przestoje_cz.Replace(" ", "");
            godziny.przestoje_pi = string.IsNullOrEmpty(godziny.przestoje_pi) ? null : godziny.przestoje_pi.Replace(" ", "");
            godziny.przestoje_so = string.IsNullOrEmpty(godziny.przestoje_so) ? null : godziny.przestoje_so.Replace(" ", "");
            godziny.przestoje_ni = string.IsNullOrEmpty(godziny.przestoje_ni) ? null : godziny.przestoje_ni.Replace(" ", "");
            godziny.dojazd_pn = string.IsNullOrEmpty(godziny.dojazd_pn) ? null : godziny.dojazd_pn.Replace(" ", "");
            godziny.dojazd_wt = string.IsNullOrEmpty(godziny.dojazd_wt) ? null : godziny.dojazd_wt.Replace(" ", "");
            godziny.dojazd_sr = string.IsNullOrEmpty(godziny.dojazd_sr) ? null : godziny.dojazd_sr.Replace(" ", "");
            godziny.dojazd_cz = string.IsNullOrEmpty(godziny.dojazd_cz) ? null : godziny.dojazd_cz.Replace(" ", "");
            godziny.dojazd_pi = string.IsNullOrEmpty(godziny.dojazd_pi) ? null : godziny.dojazd_pi.Replace(" ", "");
            godziny.dojazd_so = string.IsNullOrEmpty(godziny.dojazd_so) ? null : godziny.dojazd_so.Replace(" ", "");
            godziny.dojazd_ni = string.IsNullOrEmpty(godziny.dojazd_ni) ? null : godziny.dojazd_ni.Replace(" ", "");
            godziny.dieta_pn = godziny.dieta_pn;
            godziny.dieta_wt = godziny.dieta_wt;
            godziny.dieta_sr = godziny.dieta_sr;
            godziny.dieta_cz = godziny.dieta_cz;
            godziny.dieta_pi = godziny.dieta_pi;
            godziny.dieta_so = godziny.dieta_so;
            godziny.dieta_ni = godziny.dieta_ni;

            return godziny;
        }

        public static void ZmienGodziny(string staryTydzien, int tydzien, int budowa, int brygadzista)
        {
            using (SqlConnection connection = new SqlConnection(
              connectionString))
            {

                string queryString = string.Format("update AspNetGodziny set rokTydzien = '{0}' where rokTydzien = '{1}' and brygadzistaID = {2} and budowaID = {3}", GetTydzien(tydzien).getRokTydzien(), staryTydzien, brygadzista, budowa);
                SqlCommand command = new SqlCommand(queryString, connection);
                connection.Open();
                command.ExecuteNonQuery();
            }
        }

        public static bool SprawdzCzyAdmin(object user)
        {
            if (user == null)
            {
                return false;
            }
            if (user.ToString().Contains("mjedrzejczyk@wizualizacja.com") || user.ToString().Contains("bswierczek@poburski.pl") || user.ToString().Contains("kdrogosz@poburski.pl"))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static int GetStanowisko(int id)
        {
            int i = 0;

            using (SqlConnection connection = new SqlConnection(
              connectionString))
            {

                string queryString = string.Format("select stanowisko from pracownik WHERE idpracownika = {0}", id);
                SqlCommand command = new SqlCommand(queryString, connection);
                connection.Open();
                SqlDataReader reader = command.ExecuteReader();
                try
                {
                    while (reader.Read())
                    {
                        string stanowisko = reader[0].ToString();
                        switch (stanowisko)
                        {
                            case "BRYGADZISTA/MAJSTER":
                                i = 1;
                                break;
                            case "ZASTĘPCA MAJSTRA":
                                i = 2;
                                break;
                            case "PRACOWNIK BUDOWLANY":
                                i = 3;
                                break;
                            default:
                                break;
                        }
                    }
                }
                finally
                {
                    // Always call Close when done reading.
                    reader.Close();
                }

                return i;
            }
        }

        public static List<int> GetLata()
        {
            pobiNTRANETDataContext dc = new pobiNTRANETDataContext();
            var lata = dc.AspNetLatas.Select(l => l.rok).Distinct().ToList();
            List<int> lista = new List<int>();
            foreach (var rok in lata)
            {
                lista.Add(Convert.ToInt32(rok));
            }
            return lista;
        }

        public static dynamic GetPodpisy(DashboardViewModel dvm, Budowa budowa)
        {
            List<string> podpisy = new List<string>();
            for (int i = 0; i < dvm.Brygada.Count; i++)
            {
                pobiNTRANETDataContext dc = new pobiNTRANETDataContext();
                try
                {
                    podpisy.Add(dc.AspNetGodzinies.First(g => g.brygadzistaID == dvm.Brygadzista.Id && g.pracownikID == dvm.Brygada[i].Id && g.rokTydzien == dvm.Tydzien.getRokTydzien() && g.budowaID == budowa.Id && (!g.podpis.Equals("") && g.podpis != null)).podpis);
                }
                catch (Exception e)
                {
                    podpisy.Add("");
                    Debug.WriteLine(e.Message);
                }
            }

            return podpisy;
        }

        public static AspNetRozliczeniaBudow PoprawRozliczenie(AspNetRozliczeniaBudow rozliczenie)
        {
            //Powierzchnia Poburski
            rozliczenie.powPob_pn = Help.StringDoubleConvertible(rozliczenie.powPob_pn);
            rozliczenie.powPob_wt = Help.StringDoubleConvertible(rozliczenie.powPob_wt);
            rozliczenie.powPob_sr = Help.StringDoubleConvertible(rozliczenie.powPob_sr);
            rozliczenie.powPob_cz = Help.StringDoubleConvertible(rozliczenie.powPob_cz);
            rozliczenie.powPob_pi = Help.StringDoubleConvertible(rozliczenie.powPob_pi);
            rozliczenie.powPob_so = Help.StringDoubleConvertible(rozliczenie.powPob_so);
            //Powierzchnia Extra
            rozliczenie.powExtra_nazwa = Help.StringDoubleConvertible2(rozliczenie.powExtra_nazwa);
            rozliczenie.powExtra_pn = Help.StringDoubleConvertible(rozliczenie.powExtra_pn);
            rozliczenie.powExtra_wt = Help.StringDoubleConvertible(rozliczenie.powExtra_wt);
            rozliczenie.powExtra_sr = Help.StringDoubleConvertible(rozliczenie.powExtra_sr);
            rozliczenie.powExtra_cz = Help.StringDoubleConvertible(rozliczenie.powExtra_cz);
            rozliczenie.powExtra_pi = Help.StringDoubleConvertible(rozliczenie.powExtra_pi);
            rozliczenie.powExtra_so = Help.StringDoubleConvertible(rozliczenie.powExtra_so);
            //Godziny Poburski
            rozliczenie.godzPob_pn = Help.StringDoubleConvertible(rozliczenie.godzPob_pn);
            rozliczenie.godzPob_wt = Help.StringDoubleConvertible(rozliczenie.godzPob_wt);
            rozliczenie.godzPob_sr = Help.StringDoubleConvertible(rozliczenie.godzPob_sr);
            rozliczenie.godzPob_cz = Help.StringDoubleConvertible(rozliczenie.godzPob_cz);
            rozliczenie.godzPob_pi = Help.StringDoubleConvertible(rozliczenie.godzPob_pi);
            rozliczenie.godzPob_so = Help.StringDoubleConvertible(rozliczenie.godzPob_so);
            //Godziny Extra1
            rozliczenie.godzExtra1_nazwa = Help.StringDoubleConvertible2(rozliczenie.godzExtra1_nazwa);
            rozliczenie.godzExtra1_pn = Help.StringDoubleConvertible(rozliczenie.godzExtra1_pn);
            rozliczenie.godzExtra1_wt = Help.StringDoubleConvertible(rozliczenie.godzExtra1_wt);
            rozliczenie.godzExtra1_sr = Help.StringDoubleConvertible(rozliczenie.godzExtra1_sr);
            rozliczenie.godzExtra1_cz = Help.StringDoubleConvertible(rozliczenie.godzExtra1_cz);
            rozliczenie.godzExtra1_pi = Help.StringDoubleConvertible(rozliczenie.godzExtra1_pi);
            rozliczenie.godzExtra1_so = Help.StringDoubleConvertible(rozliczenie.godzExtra1_so);
            //Godziny Extra2
            rozliczenie.godzExtra2_nazwa = Help.StringDoubleConvertible2(rozliczenie.godzExtra2_nazwa);
            rozliczenie.godzExtra2_pn = Help.StringDoubleConvertible(rozliczenie.godzExtra2_pn);
            rozliczenie.godzExtra2_wt = Help.StringDoubleConvertible(rozliczenie.godzExtra2_wt);
            rozliczenie.godzExtra2_sr = Help.StringDoubleConvertible(rozliczenie.godzExtra2_sr);
            rozliczenie.godzExtra2_cz = Help.StringDoubleConvertible(rozliczenie.godzExtra2_cz);
            rozliczenie.godzExtra2_pi = Help.StringDoubleConvertible(rozliczenie.godzExtra2_pi);
            rozliczenie.godzExtra2_so = Help.StringDoubleConvertible(rozliczenie.godzExtra2_so);
            //Godziny Extra3
            rozliczenie.godzExtra3_nazwa = Help.StringDoubleConvertible2(rozliczenie.godzExtra3_nazwa);
            rozliczenie.godzExtra3_pn = Help.StringDoubleConvertible(rozliczenie.godzExtra3_pn);
            rozliczenie.godzExtra3_wt = Help.StringDoubleConvertible(rozliczenie.godzExtra3_wt);
            rozliczenie.godzExtra3_sr = Help.StringDoubleConvertible(rozliczenie.godzExtra3_sr);
            rozliczenie.godzExtra3_cz = Help.StringDoubleConvertible(rozliczenie.godzExtra3_cz);
            rozliczenie.godzExtra3_pi = Help.StringDoubleConvertible(rozliczenie.godzExtra3_pi);
            rozliczenie.godzExtra3_so = Help.StringDoubleConvertible(rozliczenie.godzExtra3_so);
            //Godziny Extra4
            rozliczenie.godzExtra4_nazwa = Help.StringDoubleConvertible2(rozliczenie.godzExtra4_nazwa);
            rozliczenie.godzExtra4_pn = Help.StringDoubleConvertible(rozliczenie.godzExtra4_pn);
            rozliczenie.godzExtra4_wt = Help.StringDoubleConvertible(rozliczenie.godzExtra4_wt);
            rozliczenie.godzExtra4_sr = Help.StringDoubleConvertible(rozliczenie.godzExtra4_sr);
            rozliczenie.godzExtra4_cz = Help.StringDoubleConvertible(rozliczenie.godzExtra4_cz);
            rozliczenie.godzExtra4_pi = Help.StringDoubleConvertible(rozliczenie.godzExtra4_pi);
            rozliczenie.godzExtra4_so = Help.StringDoubleConvertible(rozliczenie.godzExtra4_so);
            //Godziny dźwigu
            rozliczenie.dzwig_pn = Help.StringDoubleConvertible(rozliczenie.dzwig_pn);
            rozliczenie.dzwig_wt = Help.StringDoubleConvertible(rozliczenie.dzwig_wt);
            rozliczenie.dzwig_sr = Help.StringDoubleConvertible(rozliczenie.dzwig_sr);
            rozliczenie.dzwig_cz = Help.StringDoubleConvertible(rozliczenie.dzwig_cz);
            rozliczenie.dzwig_pi = Help.StringDoubleConvertible(rozliczenie.dzwig_pi);
            rozliczenie.dzwig_so = Help.StringDoubleConvertible(rozliczenie.dzwig_so);
            //Godziny cat
            rozliczenie.cat_pn = Help.StringDoubleConvertible(rozliczenie.cat_pn);
            rozliczenie.cat_wt = Help.StringDoubleConvertible(rozliczenie.cat_wt);
            rozliczenie.cat_sr = Help.StringDoubleConvertible(rozliczenie.cat_sr);
            rozliczenie.cat_cz = Help.StringDoubleConvertible(rozliczenie.cat_cz);
            rozliczenie.cat_pi = Help.StringDoubleConvertible(rozliczenie.cat_pi);
            rozliczenie.cat_so = Help.StringDoubleConvertible(rozliczenie.cat_so);
            //Manitou
            rozliczenie.manitou_pn = Help.StringDoubleConvertible(rozliczenie.manitou_pn);
            rozliczenie.manitou_wt = Help.StringDoubleConvertible(rozliczenie.manitou_wt);
            rozliczenie.manitou_sr = Help.StringDoubleConvertible(rozliczenie.manitou_sr);
            rozliczenie.manitou_cz = Help.StringDoubleConvertible(rozliczenie.manitou_cz);
            rozliczenie.manitou_pi = Help.StringDoubleConvertible(rozliczenie.manitou_pi);
            rozliczenie.manitou_so = Help.StringDoubleConvertible(rozliczenie.manitou_so);
            //Inne
            rozliczenie.inne_pn = Help.StringDoubleConvertible(rozliczenie.inne_pn);
            rozliczenie.inne_wt = Help.StringDoubleConvertible(rozliczenie.inne_wt);
            rozliczenie.inne_sr = Help.StringDoubleConvertible(rozliczenie.inne_sr);
            rozliczenie.inne_cz = Help.StringDoubleConvertible(rozliczenie.inne_cz);
            rozliczenie.inne_pt = Help.StringDoubleConvertible(rozliczenie.inne_pt);
            rozliczenie.inne_so = Help.StringDoubleConvertible(rozliczenie.inne_so);
            //Inne jakie?
            rozliczenie.notatki_pn = Help.StringDoubleConvertible2(rozliczenie.notatki_pn);
            rozliczenie.notatki_wt = Help.StringDoubleConvertible2(rozliczenie.notatki_wt);
            rozliczenie.notatki_sr = Help.StringDoubleConvertible2(rozliczenie.notatki_sr);
            rozliczenie.notatki_cz = Help.StringDoubleConvertible2(rozliczenie.notatki_cz);
            rozliczenie.notatki_pi = Help.StringDoubleConvertible2(rozliczenie.notatki_pi);
            rozliczenie.notatki_so = Help.StringDoubleConvertible2(rozliczenie.notatki_so);

            return rozliczenie;
        }

        public static string StringDoubleConvertible(string wartosc)
        {
            if (string.IsNullOrEmpty(wartosc))
            {
                return "0";
            }
            else
            {
                return wartosc;
            }
        }

        public static string StringDoubleConvertible2(string wartosc)
        {
            if (string.IsNullOrEmpty(wartosc))
            {
                return "";
            }
            else
            {
                return wartosc;
            }
        }

        public static dynamic GetPodpisy(List<AspNetGodziny> godziny)
        {
            List<string> podpisy = new List<string>();
            foreach (var godz in godziny)
            {
                if (!string.IsNullOrEmpty(godz.podpis))
                {
                    podpisy.Add(godz.podpis);
                }
                else
                {
                    podpisy.Add("");
                }
            }
            return podpisy;
        }

        public static akord_godziny ConvertAspNetGodzinyToAkordGodziny(AspNetGodziny godziny, int dayOfWeek, int budowa)
        {
            pobiNTRANETDataContext dc = new pobiNTRANETDataContext();

            var data = GetDayOfWeekSql(godziny.rokTydzien, dayOfWeek);

            akord_godziny result = new akord_godziny();
            result.id_dek = dc.akord_godzinies.OrderByDescending(g => g.id_dek).Select(g => g.id_dek).First() + 1;
            result.Data = DateTime.Parse(data);
            result.rok = godziny.rok;
            result.miesiac = DateTime.Parse(data).Month;
            result.dzien_miesiaca = DateTime.Parse(data).Day;
            result.tydzien = Convert.ToInt32(godziny.rokTydzien.Substring(5, 2));
            result.dzien_tygodnia = dayOfWeek;
            result.id_prac = godziny.pracownikID;
            result.idbudowy = godziny.budowaID != 0 ? godziny.budowaID : budowa;
            result.zatwierdzone = "N";
            result.dataZatwierdzenia = DateTime.Parse("1900-01-01");
            result.ktoZatwierdzil = -1;
            result.rokTydzien = godziny.rokTydzien;
            result.idUsterki = 0;
            result.czas_wpisu = DateTime.Now;
            result.rodzaj_godziny = null;
            result.dzien_wolny = godziny.obecny != null ? godziny.obecny : "brak";
            result.dodatek = 0;
            result.premia = 0;
            result.premia_extra = 0;
            result.akord_hmmm = 0;
            result.godz_deszcz = 0;

            double doGodz = 0;
            double odGodz = 0;
            double dojazd = 0;
            double przestoje = 0;
            double razem = 0;
            double razem2 = 0;

            if (godziny.obecny == "brak" || godziny.obecny == null)
            {
                switch (dayOfWeek)
                {
                    case 1:
                        doGodz = OptimizeForIntranet(godziny.do_pn);
                        odGodz = OptimizeForIntranet(godziny.od_pn);
                        dojazd = OptimizeForIntranet(godziny.dojazd_pn);
                        przestoje = OptimizeForIntranet(godziny.przestoje_pn);
                        razem = doGodz - odGodz /*+ dojazd*/ - przestoje - 0.25;
                        razem2 = doGodz - odGodz + dojazd - przestoje - 0.25;
                        result.godz_od = odGodz;
                        result.godz_do = doGodz;
                        result.dojazd = dojazd;
                        result.przestoje = przestoje;
                        result.dieta = Convert.ToInt32(godziny.dieta_pn) == 1 ? '1' : '0';
                        result.dieta_eu = Convert.ToInt32(godziny.dieta_pn) == 2 ? '1' : '0';
                        result.godz_norm = razem2 > 0 ? razem2 > 7.75 ? 8 : Convert.ToSingle(razem2) : 0;
                        result.godz_50 = razem2 > 7.75 ? Convert.ToSingle(razem2 - 8) : 0;
                        result.godz_100 = 0;
                        result.godz_akord = razem > 0 ? Convert.ToSingle(razem) : 0;
                        break;
                    case 2:
                        doGodz = OptimizeForIntranet(godziny.do_wt);
                        odGodz = OptimizeForIntranet(godziny.od_wt);
                        dojazd = OptimizeForIntranet(godziny.dojazd_wt);
                        przestoje = OptimizeForIntranet(godziny.przestoje_wt);
                        razem = doGodz - odGodz /*+ dojazd*/ - przestoje - 0.25;
                        razem2 = doGodz - odGodz + dojazd - przestoje - 0.25;
                        result.godz_od = odGodz;
                        result.godz_do = doGodz;
                        result.dojazd = dojazd;
                        result.przestoje = przestoje;
                        result.dieta = Convert.ToInt32(godziny.dieta_wt) == 1 ? '1' : '0';
                        result.dieta_eu = Convert.ToInt32(godziny.dieta_wt) == 2 ? '1' : '0';
                        result.godz_norm = razem2 > 0 ? razem2 > 7.75 ? 8 : Convert.ToSingle(razem2) : 0;
                        result.godz_50 = razem2 > 7.75 ? Convert.ToSingle(razem2 - 8) : 0;
                        result.godz_100 = 0;
                        result.godz_akord = razem > 0 ? Convert.ToSingle(razem) : 0;
                        break;
                    case 3:
                        doGodz = OptimizeForIntranet(godziny.do_sr);
                        odGodz = OptimizeForIntranet(godziny.od_sr);
                        dojazd = OptimizeForIntranet(godziny.dojazd_sr);
                        przestoje = OptimizeForIntranet(godziny.przestoje_sr);
                        razem = doGodz - odGodz /*+ dojazd*/ - przestoje - 0.25;
                        razem2 = doGodz - odGodz + dojazd - przestoje - 0.25;
                        result.godz_od = odGodz;
                        result.godz_do = doGodz;
                        result.dojazd = dojazd;
                        result.przestoje = przestoje;
                        result.dieta = Convert.ToInt32(godziny.dieta_sr) == 1 ? '1' : '0';
                        result.dieta_eu = Convert.ToInt32(godziny.dieta_sr) == 2 ? '1' : '0';
                        result.godz_norm = razem2 > 0 ? razem2 > 7.75 ? 8 : Convert.ToSingle(razem2) : 0;
                        result.godz_50 = razem2 > 7.75 ? Convert.ToSingle(razem2 - 8) : 0;
                        result.godz_100 = 0;
                        result.godz_akord = razem > 0 ? Convert.ToSingle(razem) : 0;
                        break;
                    case 4:
                        doGodz = OptimizeForIntranet(godziny.do_cz);
                        odGodz = OptimizeForIntranet(godziny.od_cz);
                        dojazd = OptimizeForIntranet(godziny.dojazd_cz);
                        przestoje = OptimizeForIntranet(godziny.przestoje_cz);
                        razem = doGodz - odGodz /*+ dojazd*/ - przestoje - 0.25;
                        razem2 = doGodz - odGodz + dojazd - przestoje - 0.25;
                        result.godz_od = odGodz;
                        result.godz_do = doGodz;
                        result.dojazd = dojazd;
                        result.przestoje = przestoje;
                        result.dieta = Convert.ToInt32(godziny.dieta_cz) == 1 ? '1' : '0';
                        result.dieta_eu = Convert.ToInt32(godziny.dieta_cz) == 2 ? '1' : '0';
                        result.godz_norm = razem2 > 0 ? razem2 > 7.75 ? 8 : Convert.ToSingle(razem2) : 0;
                        result.godz_50 = razem2 > 7.75 ? Convert.ToSingle(razem2 - 8) : 0;
                        result.godz_100 = 0;
                        result.godz_akord = razem > 0 ? Convert.ToSingle(razem) : 0;
                        break;
                    case 5:
                        doGodz = OptimizeForIntranet(godziny.do_pi);
                        odGodz = OptimizeForIntranet(godziny.od_pi);
                        dojazd = OptimizeForIntranet(godziny.dojazd_pi);
                        przestoje = OptimizeForIntranet(godziny.przestoje_pi);
                        razem = doGodz - odGodz /*+ dojazd*/ - przestoje - 0.25;
                        razem2 = doGodz - odGodz + dojazd - przestoje - 0.25;
                        result.godz_od = odGodz;
                        result.godz_do = doGodz;
                        result.dojazd = dojazd;
                        result.przestoje = przestoje;
                        result.dieta = Convert.ToInt32(godziny.dieta_pi) == 1 ? '1' : '0';
                        result.dieta_eu = Convert.ToInt32(godziny.dieta_pi) == 2 ? '1' : '0';
                        result.godz_norm = razem2 > 0 ? razem2 > 7.75 ? 8 : Convert.ToSingle(razem2) : 0;
                        result.godz_50 = razem2 > 7.75 ? Convert.ToSingle(razem2 - 8) : 0;
                        result.godz_100 = 0;
                        result.godz_akord = razem > 0 ? Convert.ToSingle(razem) : 0;
                        break;
                    case 6:
                        doGodz = OptimizeForIntranet(godziny.do_so);
                        odGodz = OptimizeForIntranet(godziny.od_so);
                        dojazd = OptimizeForIntranet(godziny.dojazd_so);
                        przestoje = OptimizeForIntranet(godziny.przestoje_so);
                        razem = doGodz - odGodz /*+ dojazd*/ - przestoje - 0.25;
                        razem2 = doGodz - odGodz + dojazd - przestoje - 0.25;
                        result.godz_od = odGodz;
                        result.godz_do = doGodz;
                        result.dojazd = dojazd;
                        result.przestoje = przestoje;
                        result.dieta = Convert.ToInt32(godziny.dieta_so) == 1 ? '1' : '0';
                        result.dieta_eu = Convert.ToInt32(godziny.dieta_so) == 2 ? '1' : '0';
                        result.godz_norm = 0;
                        result.godz_50 = razem2 > 0 ? Convert.ToSingle(razem2) : 0; //w soboty 150%
                        result.godz_100 = 0;
                        result.godz_akord = razem > 0 ? Convert.ToSingle(razem) : 0;
                        break;
                    case 7:
                        doGodz = OptimizeForIntranet(godziny.do_ni);
                        odGodz = OptimizeForIntranet(godziny.od_ni);
                        dojazd = OptimizeForIntranet(godziny.dojazd_ni);
                        przestoje = OptimizeForIntranet(godziny.przestoje_ni);
                        razem = doGodz - odGodz /*+ dojazd*/ - przestoje - 0.25;
                        razem2 = doGodz - odGodz + dojazd - przestoje - 0.25;
                        result.godz_od = odGodz;
                        result.godz_do = doGodz;
                        result.dojazd = dojazd;
                        result.przestoje = przestoje;
                        result.dieta = Convert.ToInt32(godziny.dieta_ni) == 1 ? '1' : '0';
                        result.dieta_eu = Convert.ToInt32(godziny.dieta_ni) == 2 ? '1' : '0';
                        result.godz_norm = 0;
                        result.godz_50 = 0;
                        result.godz_100 = razem2 > 0 ? Convert.ToSingle(razem2) : 0; //w niedzielę 200%
                        result.godz_akord = razem > 0 ? Convert.ToSingle(razem) : 0;
                        break;
                    default:
                        break;
                }
            }
            else
            {
                result.godz_od = 0;
                result.godz_do = 0;
                result.dojazd = 0;
                result.przestoje = 0;
                result.dieta = '0';
                result.dieta_eu = '0';
                result.godz_norm = 0;
                result.godz_50 = 0;
                result.godz_100 = 0;
                result.godz_akord = 0;
            }

            return result;
        }

        public static void StworzExcelIWyslij(AspNetRozliczeniaBudow update, string v, List<string> listaUserow, string from, string path)
        {
            using (ExcelPackage excel = new ExcelPackage())
            {
                var nazwaArkusza = "Rozliczenie budowy";
                excel.Workbook.Worksheets.Add(nazwaArkusza);
                var worksheet = excel.Workbook.Worksheets[nazwaArkusza];
                Color sumRowColumnColor = ColorTranslator.FromHtml("#B7DEE8");
                #region 1 linijka
                var headerRow = new List<object[]>()
                {
                    new object[] { string.Format("Tydzień: {0}", GetTydzien(update.tydzien_id).getRokTydzien()), string.Format("Budowa: {0}", GetBudowa(update.budowa_id).Nazwa), string.Format("Brygadzista: {0}", GetNameAndSurname(update.brygadzista_id)) }
                };
                var headerRange = "A1:" + Char.ConvertFromUtf32(headerRow[0].Length + 64) + "1";
                worksheet.Cells[headerRange].LoadFromArrays(headerRow);

                worksheet.Cells[headerRange].Style.Fill.PatternType = ExcelFillStyle.Solid;
                worksheet.Cells[headerRange].Style.Fill.BackgroundColor.SetColor(sumRowColumnColor);
                #endregion

                #region powierzchnia tytuł
                var Row2 = new List<object[]>()
                {
                    new object[] { "Powierzchnia zrobiona przez", "Poniedziałek", "Wtorek", "Środa", "Czwartek", "Piątek", "Sobota", "SUMA" }
                };
                var row2Range = "A2:" + Char.ConvertFromUtf32(Row2[0].Length + 64) + "2";
                worksheet.Cells[row2Range].LoadFromArrays(Row2);
                worksheet.Cells[row2Range].Style.Font.Bold = true;

                worksheet.Cells[row2Range].Style.Fill.PatternType = ExcelFillStyle.Solid;
                worksheet.Cells[row2Range].Style.Fill.BackgroundColor.SetColor(sumRowColumnColor);
                #endregion

                #region powierzchnia Poburski
                double powPob_suma = Convert.ToDouble(update.powPob_pn) + Convert.ToDouble(update.powPob_wt) + Convert.ToDouble(update.powPob_sr) + Convert.ToDouble(update.powPob_cz) + Convert.ToDouble(update.powPob_pi) + Convert.ToDouble(update.powPob_so);
                var Row3 = new List<object[]>()
                {
                    new object[] { "Poburski", Convert.ToDouble(update.powPob_pn), Convert.ToDouble(update.powPob_wt), Convert.ToDouble(update.powPob_sr), Convert.ToDouble(update.powPob_cz), Convert.ToDouble(update.powPob_pi), Convert.ToDouble(update.powPob_so), powPob_suma }
                };
                var row3Range = "A3:" + Char.ConvertFromUtf32(Row3[0].Length + 64) + "3";
                worksheet.Cells[row3Range].LoadFromArrays(Row3);
                #endregion

                #region powierzchnia Extra
                double powExtra_suma = Convert.ToDouble(update.powExtra_pn) + Convert.ToDouble(update.powExtra_wt) + Convert.ToDouble(update.powExtra_sr) + Convert.ToDouble(update.powExtra_cz) + Convert.ToDouble(update.powExtra_pi) + Convert.ToDouble(update.powExtra_so);
                var Row4 = new List<object[]>()
                {
                    new object[] { update.powExtra_nazwa, Convert.ToDouble(update.powExtra_pn), Convert.ToDouble(update.powExtra_wt), Convert.ToDouble(update.powExtra_sr), Convert.ToDouble(update.powExtra_cz), Convert.ToDouble(update.powExtra_pi), Convert.ToDouble(update.powExtra_so), powExtra_suma }
                };
                var row4Range = "A4:" + Char.ConvertFromUtf32(Row4[0].Length + 64) + "4";
                worksheet.Cells[row4Range].LoadFromArrays(Row4);
                #endregion

                #region powierzchnia Suma
                var Row5 = new List<object[]>()
                {
                    new object[] { "SUMA", Convert.ToDouble(update.powPob_pn) + Convert.ToDouble(update.powExtra_pn), Convert.ToDouble(update.powPob_wt) + Convert.ToDouble(update.powExtra_wt), Convert.ToDouble(update.powPob_sr) + Convert.ToDouble(update.powExtra_sr), Convert.ToDouble(update.powPob_cz) + Convert.ToDouble(update.powExtra_cz), Convert.ToDouble(update.powPob_pi) + Convert.ToDouble(update.powExtra_pi), Convert.ToDouble(update.powPob_so) + Convert.ToDouble(update.powExtra_so) }
                };
                var row5Range = "A5:" + Char.ConvertFromUtf32(Row5[0].Length + 64) + "5";
                worksheet.Cells[row5Range].LoadFromArrays(Row5);
                worksheet.Cells[row5Range].Style.Font.Bold = true;
                #endregion

                #region godziny tytuł
                var Row6 = new List<object[]>()
                {
                    new object[] { "Godziny", "Poniedziałek", "Wtorek", "Środa", "Czwartek", "Piątek", "Sobota", "SUMA" }
                };
                var row6Range = "A6:" + Char.ConvertFromUtf32(Row6[0].Length + 64) + "6";
                worksheet.Cells[row6Range].LoadFromArrays(Row6);
                worksheet.Cells[row6Range].Style.Font.Bold = true;

                worksheet.Cells[row6Range].Style.Fill.PatternType = ExcelFillStyle.Solid;
                worksheet.Cells[row6Range].Style.Fill.BackgroundColor.SetColor(sumRowColumnColor);
                #endregion

                #region godziny Poburski
                double godzPob_suma = Convert.ToDouble(update.godzPob_pn) + Convert.ToDouble(update.godzPob_wt) + Convert.ToDouble(update.godzPob_sr) + Convert.ToDouble(update.godzPob_cz) + Convert.ToDouble(update.godzPob_pi) + Convert.ToDouble(update.godzPob_so);
                var Row7 = new List<object[]>()
                {
                    new object[] { "Poburski", Convert.ToDouble(update.godzPob_pn), Convert.ToDouble(update.godzPob_wt), Convert.ToDouble(update.godzPob_sr), Convert.ToDouble(update.godzPob_cz), Convert.ToDouble(update.godzPob_pi), Convert.ToDouble(update.godzPob_so), Convert.ToDouble(godzPob_suma) }
                };
                var row7Range = "A7:" + Char.ConvertFromUtf32(Row7[0].Length + 64) + "7";
                worksheet.Cells[row7Range].LoadFromArrays(Row7);
                #endregion

                #region godziny Extra 1
                double godzExtra1_suma = Convert.ToDouble(update.godzExtra1_pn) + Convert.ToDouble(update.godzExtra1_wt) + Convert.ToDouble(update.godzExtra1_sr) + Convert.ToDouble(update.godzExtra1_cz) + Convert.ToDouble(update.godzExtra1_pi) + Convert.ToDouble(update.godzExtra1_so);
                var Row8 = new List<object[]>()
                {
                    new object[] { update.godzExtra1_nazwa, Convert.ToDouble(update.godzExtra1_pn), Convert.ToDouble(update.godzExtra1_wt), Convert.ToDouble(update.godzExtra1_sr), Convert.ToDouble(update.godzExtra1_cz), Convert.ToDouble(update.godzExtra1_pi), Convert.ToDouble(update.godzExtra1_so), godzExtra1_suma }
                };
                var row8Range = "A8:" + Char.ConvertFromUtf32(Row8[0].Length + 64) + "8";
                worksheet.Cells[row8Range].LoadFromArrays(Row8);
                #endregion

                #region godziny Extra 2
                double godzExtra2_suma = Convert.ToDouble(update.godzExtra2_pn) + Convert.ToDouble(update.godzExtra2_wt) + Convert.ToDouble(update.godzExtra2_sr) + Convert.ToDouble(update.godzExtra2_cz) + Convert.ToDouble(update.godzExtra2_pi) + Convert.ToDouble(update.godzExtra2_so);
                var Row9 = new List<object[]>()
                {
                    new object[] { update.godzExtra2_nazwa, Convert.ToDouble(update.godzExtra2_pn), Convert.ToDouble(update.godzExtra2_wt), Convert.ToDouble(update.godzExtra2_sr), Convert.ToDouble(update.godzExtra2_cz), Convert.ToDouble(update.godzExtra2_pi), Convert.ToDouble(update.godzExtra2_so), godzExtra2_suma }
                };
                var row9Range = "A9:" + Char.ConvertFromUtf32(Row9[0].Length + 64) + "9";
                worksheet.Cells[row9Range].LoadFromArrays(Row9);
                #endregion

                #region godziny Extra 3
                double godzExtra3_suma = Convert.ToDouble(update.godzExtra3_pn) + Convert.ToDouble(update.godzExtra3_wt) + Convert.ToDouble(update.godzExtra3_sr) + Convert.ToDouble(update.godzExtra3_cz) + Convert.ToDouble(update.godzExtra3_pi) + Convert.ToDouble(update.godzExtra3_so);
                var Row10 = new List<object[]>()
                {
                    new object[] { update.godzExtra3_nazwa, Convert.ToDouble(update.godzExtra3_pn), Convert.ToDouble(update.godzExtra3_wt), Convert.ToDouble(update.godzExtra3_sr), Convert.ToDouble(update.godzExtra3_cz), Convert.ToDouble(update.godzExtra3_pi), Convert.ToDouble(update.godzExtra3_so), godzExtra3_suma }
                };
                var row10Range = "A10:" + Char.ConvertFromUtf32(Row10[0].Length + 64) + "10";
                worksheet.Cells[row10Range].LoadFromArrays(Row10);
                #endregion

                #region godziny Extra 4
                double godzExtra4_suma = Convert.ToDouble(update.godzExtra4_pn) + Convert.ToDouble(update.godzExtra4_wt) + Convert.ToDouble(update.godzExtra4_sr) + Convert.ToDouble(update.godzExtra4_cz) + Convert.ToDouble(update.godzExtra4_pi) + Convert.ToDouble(update.godzExtra4_so);
                var Row11 = new List<object[]>()
                {
                    new object[] { update.godzExtra4_nazwa, Convert.ToDouble(update.godzExtra4_pn), Convert.ToDouble(update.godzExtra4_wt), Convert.ToDouble(update.godzExtra4_sr), Convert.ToDouble(update.godzExtra4_cz), Convert.ToDouble(update.godzExtra4_pi), Convert.ToDouble(update.godzExtra4_so), godzExtra4_suma }
                };
                var row11Range = "A11:" + Char.ConvertFromUtf32(Row11[0].Length + 64) + "11";
                worksheet.Cells[row11Range].LoadFromArrays(Row11);
                #endregion

                #region godziny Suma
                var Row12 = new List<object[]>()
                {
                    new object[] { "SUMA", Convert.ToDouble(update.godzPob_pn)+ Convert.ToDouble(update.godzExtra1_pn)+ Convert.ToDouble(update.godzExtra2_pn)+ Convert.ToDouble(update.godzExtra3_pn)+ Convert.ToDouble(update.godzExtra4_pn), Convert.ToDouble(update.godzPob_wt) + Convert.ToDouble(update.godzExtra1_wt) + Convert.ToDouble(update.godzExtra2_wt) + Convert.ToDouble(update.godzExtra3_wt) + Convert.ToDouble(update.godzExtra4_wt), Convert.ToDouble(update.godzPob_sr) + Convert.ToDouble(update.godzExtra1_sr) + Convert.ToDouble(update.godzExtra2_sr) + Convert.ToDouble(update.godzExtra3_sr) + Convert.ToDouble(update.godzExtra4_sr), Convert.ToDouble(update.godzPob_cz) + Convert.ToDouble(update.godzExtra1_cz) + Convert.ToDouble(update.godzExtra2_cz) + Convert.ToDouble(update.godzExtra3_cz) + Convert.ToDouble(update.godzExtra4_cz), Convert.ToDouble(update.godzPob_pi) + Convert.ToDouble(update.godzExtra1_pi) + Convert.ToDouble(update.godzExtra2_pi) + Convert.ToDouble(update.godzExtra3_pi) + Convert.ToDouble(update.godzExtra4_pi), Convert.ToDouble(update.godzPob_so) + Convert.ToDouble(update.godzExtra1_so) + Convert.ToDouble(update.godzExtra2_so) + Convert.ToDouble(update.godzExtra3_so) + Convert.ToDouble(update.godzExtra4_so) }
                };
                var row12Range = "A12:" + Char.ConvertFromUtf32(Row12[0].Length + 64) + "12";
                worksheet.Cells[row12Range].LoadFromArrays(Row12);
                worksheet.Cells[row12Range].Style.Font.Bold = true;
                #endregion

                #region godziny sprzętu tytuł
                var Row13 = new List<object[]>()
                {
                    new object[] { "Godziny sprzętu", "Poniedziałek", "Wtorek", "Środa", "Czwartek", "Piątek", "Sobota", "SUMA" }
                };
                var row13Range = "A13:" + Char.ConvertFromUtf32(Row13[0].Length + 64) + "13";
                worksheet.Cells[row13Range].LoadFromArrays(Row13);
                worksheet.Cells[row13Range].Style.Font.Bold = true;

                worksheet.Cells[row13Range].Style.Fill.PatternType = ExcelFillStyle.Solid;
                worksheet.Cells[row13Range].Style.Fill.BackgroundColor.SetColor(sumRowColumnColor);
                #endregion

                #region godziny sprzętu dźwig
                double dzwig_suma = Convert.ToDouble(update.dzwig_pn) + Convert.ToDouble(update.dzwig_wt) + Convert.ToDouble(update.dzwig_sr) + Convert.ToDouble(update.dzwig_cz) + Convert.ToDouble(update.dzwig_pi) + Convert.ToDouble(update.dzwig_so);
                var Row14 = new List<object[]>()
                {
                    new object[] { "Dźwig", Convert.ToDouble(update.dzwig_pn), Convert.ToDouble(update.dzwig_wt), Convert.ToDouble(update.dzwig_sr), Convert.ToDouble(update.dzwig_cz), Convert.ToDouble(update.dzwig_pi), Convert.ToDouble(update.dzwig_so), dzwig_suma }
                };
                var row14Range = "A14:" + Char.ConvertFromUtf32(Row14[0].Length + 64) + "14";
                worksheet.Cells[row14Range].LoadFromArrays(Row14);
                #endregion

                #region godziny sprzętu cat
                double cat_suma = Convert.ToDouble(update.cat_pn) + Convert.ToDouble(update.cat_wt) + Convert.ToDouble(update.cat_sr) + Convert.ToDouble(update.cat_cz) + Convert.ToDouble(update.cat_pi) + Convert.ToDouble(update.cat_so);
                var Row15 = new List<object[]>()
                {
                    new object[] { "Cat", Convert.ToDouble(update.cat_pn), Convert.ToDouble(update.cat_wt), Convert.ToDouble(update.cat_sr), Convert.ToDouble(update.cat_cz), Convert.ToDouble(update.cat_pi), Convert.ToDouble(update.cat_so), cat_suma }
                };
                var row15Range = "A15:" + Char.ConvertFromUtf32(Row15[0].Length + 64) + "15";
                worksheet.Cells[row15Range].LoadFromArrays(Row15);
                #endregion

                #region godziny sprzętu manitou
                double manitou_suma = Convert.ToDouble(update.manitou_pn) + Convert.ToDouble(update.manitou_wt) + Convert.ToDouble(update.manitou_sr) + Convert.ToDouble(update.manitou_cz) + Convert.ToDouble(update.manitou_pi) + Convert.ToDouble(update.manitou_so);
                var Row16 = new List<object[]>()
                {
                    new object[] { "Manitou", Convert.ToDouble(update.manitou_pn), Convert.ToDouble(update.manitou_wt), Convert.ToDouble(update.manitou_sr), Convert.ToDouble(update.manitou_cz), Convert.ToDouble(update.manitou_pi), Convert.ToDouble(update.manitou_so), manitou_suma }
                };
                var row16Range = "A16:" + Char.ConvertFromUtf32(Row16[0].Length + 64) + "16";
                worksheet.Cells[row16Range].LoadFromArrays(Row16);
                #endregion

                #region godziny sprzętu inne
                double inne_suma = Convert.ToDouble(update.inne_pn) + Convert.ToDouble(update.inne_wt) + Convert.ToDouble(update.inne_sr) + Convert.ToDouble(update.inne_cz) + Convert.ToDouble(update.inne_pt) + Convert.ToDouble(update.inne_so);
                var Row17 = new List<object[]>()
                {
                    new object[] { "Inne", Convert.ToDouble(update.inne_pn), Convert.ToDouble(update.inne_wt), Convert.ToDouble(update.inne_sr), Convert.ToDouble(update.inne_cz), Convert.ToDouble(update.inne_pt), Convert.ToDouble(update.inne_so), inne_suma }
                };
                var row17Range = "A17:" + Char.ConvertFromUtf32(Row17[0].Length + 64) + "17";
                worksheet.Cells[row17Range].LoadFromArrays(Row17);
                #endregion

                #region godziny sprzętu jakie?
                var Row18 = new List<object[]>()
                {
                    new object[] { "Inne jakie?", update.notatki_pn, update.notatki_wt, update.notatki_sr, update.notatki_cz, update.notatki_pi, update.notatki_so }
                };
                var row18Range = "A18:" + Char.ConvertFromUtf32(Row18[0].Length + 64) + "18";
                worksheet.Cells[row18Range].LoadFromArrays(Row18);
                #endregion

                #region Podpis
                var Row19 = new List<object[]>()
                {
                    new object[] { "Link do podpisu: ", @"dekadowki.poburski.pl/Content/signatures/" + update.podpis }
                };
                var row19Range = "A19:" + Char.ConvertFromUtf32(Row19[0].Length + 64) + "19";
                worksheet.Cells[row19Range].LoadFromArrays(Row19);
                #endregion

                #region Style
                worksheet.Cells.AutoFitColumns();

                var allRange = "A1:H18";
                worksheet.Cells[allRange].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                worksheet.Cells[allRange].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                worksheet.Cells[allRange].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                worksheet.Cells[allRange].Style.Border.Left.Style = ExcelBorderStyle.Thin;

                worksheet.Cells[allRange].Style.Border.Bottom.Color.SetColor(Color.Black);
                worksheet.Cells[allRange].Style.Border.Top.Color.SetColor(Color.Black);
                worksheet.Cells[allRange].Style.Border.Right.Color.SetColor(Color.Black);
                worksheet.Cells[allRange].Style.Border.Left.Color.SetColor(Color.Black);

                var sumaRange = "H1:H18";
                worksheet.Cells[sumaRange].Style.Font.Bold = true;
                #endregion

                path += string.Format("{0}-{1}-{2}-{3}.xlsx", "rozliczenie_bud" + update.brygadzista_id, update.budowa_id, update.tydzien_id, DateTime.Now.ToShortDateString());
                FileInfo excelFile = new FileInfo(path);
                excel.SaveAs(excelFile);

                foreach (var to in listaUserow)
                {
                    string To = to;
                    string From = "mjedrzejczyk@wizualizacja.com";
                    string Subject = "Rozliczenie budowy - " + GetNameAndSurname(update.brygadzista_id);
                    string Body = string.Format("<html><body><p>W załączniku raport z budowy.</p><p>Budowa: {0}</p><p>Tydzień: {1}</p><p>Brygadzista: {2}</p></body><html>", GetBudowa(update.budowa_id).Nazwa, GetTydzien(update.tydzien_id).getRokTydzien(), GetNameAndSurname(update.brygadzista_id));

                    MailMessage msg = new MailMessage(From, To, Subject, Body); // create the email message
                    msg.IsBodyHtml = true;
                    try // attachments
                    {
                        msg.Attachments.Add(new Attachment(path));
                    }
                    catch (Exception ex2)
                    {
                        Console.WriteLine(ex2);
                    }

                    SmtpClient client = new SmtpClient();
                    client.UseDefaultCredentials = false;
                    client.Credentials = new System.Net.NetworkCredential("mjedrzejczyk@wizualizacja.com", "WizMJ2018");
                    client.Port = 587; // You can use Port 25 if 587 is blocked (mine is!)
                    client.Host = "smtp.office365.com";
                    client.DeliveryMethod = SmtpDeliveryMethod.Network;
                    client.EnableSsl = true;
                    try
                    {
                        client.Send(msg);
                    }
                    catch (Exception ex)
                    {
                        Debug.WriteLine(ex.Message);
                    }

                    client.Dispose();
                    msg.Dispose();
                }
            }
        }

        private static double OptimizeForIntranet(string godzina)
        {
            if (string.IsNullOrEmpty(godzina))
            {
                return 0;
            }
            else if (string.IsNullOrEmpty(godzina.Replace(" ", "")))
            {
                return 0;
            }

            string str = "";
            //Konwersja do dobrego wyświetlania w intranecie
            try
            {
                str = godzina.ToString();
                //(.30 = .50);
                if (str.Contains(".30") || str.Contains(".3"))
                {
                    str = (str.Substring(0, 2) + ".50").Replace("..", ".");
                }
                //(.15 = .25);
                if (str.Contains(".15"))
                {
                    str = (str.Substring(0, 2) + ".25").Replace("..", ".");
                }
                //(.45 = .75);
                if (str.Contains(".45"))
                {
                    str = (str.Substring(0, 2) + ".75").Replace("..", ".");
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
            }

            return godzina != null ? Convert.ToDouble(str.Replace(" ", "")) : 0;
        }

        private static string GetDayOfWeekSql(string week, int dayOfWeek)
        {
            string result = "";

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                string queryString = string.Format("SELECT TOP 1 data, dzien_tygodnia FROM lata WHERE rokTydzien = '{0}' and dzien_tygodnia = {1} ORDER BY dzien_tygodnia desc", week, dayOfWeek);
                SqlCommand command = new SqlCommand(queryString, connection);
                connection.Open();
                SqlDataReader reader = command.ExecuteReader();
                try
                {
                    while (reader.Read())
                    {
                        result = reader[0].ToString();
                    }
                }
                finally
                {
                    // Always call Close when done reading.
                    reader.Close();
                }
            }

            try
            {
                return DateTime.Parse(result).ToString("yyyy-MM-dd");
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
                return DateTime.Now.ToString("yyyy-MM-dd");
            }

        }

        public static void SaveWeekToDatabase(DashboardViewModel dvm, Budowa budowa, string path, List<string> imageData, string serverPath, Tydzien tydzienTmp)
        {
            try
            {
                var json = JsonConvert.SerializeObject(dvm, Formatting.Indented);
                var filename = AppDomain.CurrentDomain.BaseDirectory + "Logi\\" + "Dashboard\\" + dvm.Brygadzista.getNazwa() + DateTime.Now.ToString("dd-MM-yyyy") + ".txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + Environment.NewLine + json.ToString() + Environment.NewLine + ":::Tydzień:::" + tydzienTmp.getRokTydzien() + Environment.NewLine + Environment.NewLine + ":::Budowa:::" + budowa.Nazwa + Environment.NewLine + "***************************************");
                sw.Close();
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
            }

            pobiNTRANETDataContext dataContext = new pobiNTRANETDataContext();

            for (int i = 0; i < dvm.Brygada.Count; i++)
            {
                AspNetGodziny aspnetgodziny = new AspNetGodziny();

                bool insert = true;
                string fileName = string.Format("{0}-{1}-{2}-{3}.png", dvm.Brygadzista.Id, dvm.Brygada[i].Id, dvm.Tydzien.getRokTydzien(), DateTime.Now.ToShortDateString());
                string fileNameWitPath = Path.Combine(path, fileName);
                string currentPodpis = null;
                //czy pusty
                try
                {
                    if (imageData != null)
                    {
                        if (!string.IsNullOrEmpty(imageData[i]) && imageData[i] != pustyPodpis)
                        {
                            using (FileStream fs = new FileStream(fileNameWitPath, FileMode.Create))
                            {
                                using (BinaryWriter bw = new BinaryWriter(fs))
                                {
                                    byte[] data = Convert.FromBase64String(imageData[i]);
                                    bw.Write(data);
                                    bw.Close();
                                }
                                fs.Close();
                            }
                            currentPodpis = fileName;
                        }
                    }
                }
                catch (Exception e)
                {
                    //Brak podpisu lub błąd podpisów
                    Debug.WriteLine(e.Message);
                }

                var tydzien = dvm.Tydzien.getRokTydzien();
                var pracownik = dvm.Brygada[i];
                if (dataContext.AspNetGodzinies.Any(g => g.rokTydzien == tydzien && g.pracownikID == pracownik.Id && g.brygadzistaID == dvm.Brygadzista.Id && g.budowaID == budowa.Id))
                {
                    insert = false;
                    aspnetgodziny = dataContext.AspNetGodzinies.First(g => g.rokTydzien == tydzien && g.pracownikID == pracownik.Id && g.brygadzistaID == dvm.Brygadzista.Id && g.budowaID == budowa.Id);
                }

                if (insert)
                {
                    aspnetgodziny.data_wprowadzenia = DateTime.Now.Date.ToString("dd-MM-yyyy hh:mm");
                    if (i == 0)
                    {
                        aspnetgodziny.brygadzistaID = pracownik.Id;
                    }
                    else
                    {
                        aspnetgodziny.brygadzistaID = dvm.Brygadzista.Id;
                    }
                    aspnetgodziny.budowaID = budowa.Id;
                    aspnetgodziny.rok = dvm.Tydzien.getRok();
                    aspnetgodziny.podpis = "";
                    aspnetgodziny.rokTydzien = tydzien;
                    aspnetgodziny.pracownikID = dvm.Brygada[i].Id;
                }
                else
                {
                    aspnetgodziny.data_modyfikacji = DateTime.Now.Date.ToString("dd-MM-yyyy hh:mm");
                }

                //podpis
                if (string.IsNullOrEmpty(aspnetgodziny.podpis)) { aspnetgodziny.podpis = currentPodpis; };
                //od
                aspnetgodziny.od_pn = dvm.DniTygodnia[i].od_pn;
                aspnetgodziny.od_wt = dvm.DniTygodnia[i].od_wt;
                aspnetgodziny.od_sr = dvm.DniTygodnia[i].od_sr;
                aspnetgodziny.od_cz = dvm.DniTygodnia[i].od_cz;
                aspnetgodziny.od_pi = dvm.DniTygodnia[i].od_pi;
                aspnetgodziny.od_so = dvm.DniTygodnia[i].od_so;
                aspnetgodziny.od_ni = dvm.DniTygodnia[i].od_ni;
                //do
                aspnetgodziny.do_pn = dvm.DniTygodnia[i].do_pn;
                aspnetgodziny.do_wt = dvm.DniTygodnia[i].do_wt;
                aspnetgodziny.do_sr = dvm.DniTygodnia[i].do_sr;
                aspnetgodziny.do_cz = dvm.DniTygodnia[i].do_cz;
                aspnetgodziny.do_pi = dvm.DniTygodnia[i].do_pi;
                aspnetgodziny.do_so = dvm.DniTygodnia[i].do_so;
                aspnetgodziny.do_ni = dvm.DniTygodnia[i].do_ni;
                //przestoje
                aspnetgodziny.przestoje_pn = dvm.DniTygodnia[i].przestoje_pn;
                aspnetgodziny.przestoje_wt = dvm.DniTygodnia[i].przestoje_wt;
                aspnetgodziny.przestoje_sr = dvm.DniTygodnia[i].przestoje_sr;
                aspnetgodziny.przestoje_cz = dvm.DniTygodnia[i].przestoje_cz;
                aspnetgodziny.przestoje_pi = dvm.DniTygodnia[i].przestoje_pi;
                aspnetgodziny.przestoje_so = dvm.DniTygodnia[i].przestoje_so;
                aspnetgodziny.przestoje_ni = dvm.DniTygodnia[i].przestoje_ni;
                //dojazd
                aspnetgodziny.dojazd_pn = dvm.DniTygodnia[i].dojazd_pn;
                aspnetgodziny.dojazd_wt = dvm.DniTygodnia[i].dojazd_wt;
                aspnetgodziny.dojazd_sr = dvm.DniTygodnia[i].dojazd_sr;
                aspnetgodziny.dojazd_cz = dvm.DniTygodnia[i].dojazd_cz;
                aspnetgodziny.dojazd_pi = dvm.DniTygodnia[i].dojazd_pi;
                aspnetgodziny.dojazd_so = dvm.DniTygodnia[i].dojazd_so;
                aspnetgodziny.dojazd_ni = dvm.DniTygodnia[i].dojazd_ni;
                //dieta
                aspnetgodziny.dieta_pn = dvm.DniTygodnia[i].dieta_pn.ToString();
                aspnetgodziny.dieta_wt = dvm.DniTygodnia[i].dieta_wt.ToString();
                aspnetgodziny.dieta_sr = dvm.DniTygodnia[i].dieta_sr.ToString();
                aspnetgodziny.dieta_cz = dvm.DniTygodnia[i].dieta_cz.ToString();
                aspnetgodziny.dieta_pi = dvm.DniTygodnia[i].dieta_pi.ToString();
                aspnetgodziny.dieta_so = dvm.DniTygodnia[i].dieta_so.ToString();
                aspnetgodziny.dieta_ni = dvm.DniTygodnia[i].dieta_ni.ToString();
                //obecność
                aspnetgodziny.obecny = dvm.DniTygodnia[i].obecny;
                aspnetgodziny.obecny_pn = dvm.DniTygodnia[i].obecny_pn;
                aspnetgodziny.obecny_wt = dvm.DniTygodnia[i].obecny_wt;
                aspnetgodziny.obecny_sr = dvm.DniTygodnia[i].obecny_sr;
                aspnetgodziny.obecny_cz = dvm.DniTygodnia[i].obecny_cz;
                aspnetgodziny.obecny_pi = dvm.DniTygodnia[i].obecny_pi;
                aspnetgodziny.obecny_so = dvm.DniTygodnia[i].obecny_so;
                aspnetgodziny.obecny_ni = dvm.DniTygodnia[i].obecny_ni;

                aspnetgodziny.export = 0;

                try
                {
                    var json = JsonConvert.SerializeObject(aspnetgodziny, Formatting.Indented);
                    var filename = AppDomain.CurrentDomain.BaseDirectory + "Logi\\" + "Dashboard\\" + dvm.Brygadzista.getNazwa() + DateTime.Now.ToString("dd-MM-yyyy") + "-potwierdzenie.txt";
                    var sw = new System.IO.StreamWriter(filename, true);
                    sw.WriteLine(DateTime.Now.ToString() + Environment.NewLine + json.ToString() + Environment.NewLine + ":::Tydzień:::" + tydzienTmp.getRokTydzien() + Environment.NewLine + Environment.NewLine + ":::Budowa:::" + budowa.Nazwa + Environment.NewLine + "***************************************");
                    sw.Close();
                }
                catch (Exception e)
                {
                    Debug.WriteLine(e.Message);
                }

                if (budowa.Id == 0)
                {
                    aspnetgodziny.komentarz = budowa.Nazwa;
                }

                if (insert)
                {
                    dataContext.AspNetGodzinies.InsertOnSubmit(aspnetgodziny);
                }
                dataContext.SubmitChanges();
            }
        }


        public static void BlockWeekIfNotNeeded()
        {

            if (DateTime.Now.DayOfWeek != DayOfWeek.Monday)
            {
                List<Tydzien> wszystkieTygodnie = GetTygodnie(DateTime.Now.Year);
                int currentWeek = GetIso8601WeekOfYear(DateTime.Now);
                int currentYear = DateTime.Now.Year;

                foreach (var tydzien in wszystkieTygodnie)
                {
                    if (tydzien.getRokTydzien() != string.Format("{0}.{1}", currentYear, currentWeek.ToString().PadLeft(2, '0')) && IfWeekHasCurrentMonth(tydzien))
                    {
                        if (tydzien.getZablokowany() == 0)
                        {
                            BlockWeek(tydzien);
                        }
                    }
                    else
                    {
                        break;
                    }
                }
            }
        }
        private static bool IfWeekHasCurrentMonth(Tydzien tydzien)
        {
            var firstDay = GetDayOfWeek(tydzien.getRokTydzien(), 1);
            var lastDay = GetDayOfWeek(tydzien.getRokTydzien(), 7);
            var monthFirstDay = Convert.ToInt32(firstDay.Substring(3, 2));
            var monthLastDay = Convert.ToInt32(lastDay.Substring(3, 2));
            if (monthFirstDay > DateTime.Now.Month + 1 || monthLastDay > DateTime.Now.Month + 1) //Ten miesiąc + następny
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private static void BlockWeek(Tydzien tydzien)
        {
            using (SqlConnection connection = new SqlConnection(
              connectionString))
            {

                string queryString = string.Format("UPDATE AspNetLata SET zablokowany = 1 WHERE idtygodnia = {0}", tydzien.getId());
                SqlCommand command = new SqlCommand(queryString, connection);
                connection.Open();
                command.ExecuteNonQuery();

            }
        }

        public static int GetIso8601WeekOfYear(DateTime time)
        {
            // Seriously cheat.  If its Monday, Tuesday or Wednesday, then it'll 
            // be the same week# as whatever Thursday, Friday or Saturday are,
            // and we always get those right
            DayOfWeek day = CultureInfo.InvariantCulture.Calendar.GetDayOfWeek(time);
            if (day >= DayOfWeek.Monday && day <= DayOfWeek.Wednesday)
            {
                time = time.AddDays(3);
            }

            // Return the week of our adjusted day
            return CultureInfo
                .InvariantCulture
                .Calendar
                .GetWeekOfYear(time, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);
        }

        public static List<DniTygodnia> GetDniTygodnia(int liczebnosc, Tydzien aktualnyTydzien, int idusera, List<Brygada> brygada, int budowa)
        {
            try
            {
                pobiNTRANETDataContext dataContext = new pobiNTRANETDataContext();
                List<DniTygodnia> listaDniTygodnia = new List<DniTygodnia>();
                for (int i = 0; i < liczebnosc; i++)
                {
                    DniTygodnia dniTygodnia = null;
                    if (dataContext.AspNetGodzinies.Any(g => g.rokTydzien == aktualnyTydzien.getRokTydzien() && g.brygadzistaID == idusera && g.pracownikID == brygada[i].Id && g.budowaID == budowa))
                    {
                        var godziny = dataContext.AspNetGodzinies.First(g => g.rokTydzien == aktualnyTydzien.getRokTydzien() && g.brygadzistaID == idusera && g.pracownikID == brygada[i].Id && g.budowaID == budowa);
                        godziny = PoprawGodziny(godziny);
                        dniTygodnia = getWeekFromDatabase(godziny);
                    }
                    else
                    {
                        dniTygodnia = GetUserSettings(idusera);
                    }
                    dniTygodnia.data_pn = GetDayOfWeek(aktualnyTydzien.getRokTydzien(), 1);
                    dniTygodnia.data_wt = GetDayOfWeek(aktualnyTydzien.getRokTydzien(), 2);
                    dniTygodnia.data_sr = GetDayOfWeek(aktualnyTydzien.getRokTydzien(), 3);
                    dniTygodnia.data_cz = GetDayOfWeek(aktualnyTydzien.getRokTydzien(), 4);
                    dniTygodnia.data_pi = GetDayOfWeek(aktualnyTydzien.getRokTydzien(), 5);
                    dniTygodnia.data_so = GetDayOfWeek(aktualnyTydzien.getRokTydzien(), 6);
                    dniTygodnia.data_ni = GetDayOfWeek(aktualnyTydzien.getRokTydzien(), 7);

                    listaDniTygodnia.Add(dniTygodnia);
                }

                return listaDniTygodnia;
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
                return new List<DniTygodnia>();
            }
        }

        public static List<Tydzien> GetTygodnieZGodzin(List<AspNetGodziny> godziny)
        {
            List<Tydzien> listaTygodni = new List<Tydzien>();

            var listaId = godziny.OrderBy(g => g.rokTydzien).Select(g => g.rokTydzien).Distinct().ToList();
            foreach (var obj in listaId)
            {
                Tydzien tydzien = GetTydzien(obj);
                listaTygodni.Add(tydzien);
            }

            return listaTygodni;
        }

        private static DniTygodnia getWeekFromDatabase(AspNetGodziny godziny)
        {
            DniTygodnia dniTygodnia = new DniTygodnia();
            godziny = PoprawGodziny(godziny);
            dniTygodnia.obecny = godziny.obecny;
            dniTygodnia.od_pn = godziny.od_pn;
            dniTygodnia.od_wt = godziny.od_wt;
            dniTygodnia.od_sr = godziny.od_sr;
            dniTygodnia.od_cz = godziny.od_cz;
            dniTygodnia.od_pi = godziny.od_pi;
            dniTygodnia.od_so = godziny.od_so;
            dniTygodnia.od_ni = godziny.od_ni;
            dniTygodnia.do_pn = godziny.do_pn;
            dniTygodnia.do_wt = godziny.do_wt;
            dniTygodnia.do_sr = godziny.do_sr;
            dniTygodnia.do_cz = godziny.do_cz;
            dniTygodnia.do_pi = godziny.do_pi;
            dniTygodnia.do_so = godziny.do_so;
            dniTygodnia.do_ni = godziny.do_ni;
            dniTygodnia.przestoje_pn = godziny.przestoje_pn;
            dniTygodnia.przestoje_wt = godziny.przestoje_wt;
            dniTygodnia.przestoje_sr = godziny.przestoje_sr;
            dniTygodnia.przestoje_cz = godziny.przestoje_cz;
            dniTygodnia.przestoje_pi = godziny.przestoje_pi;
            dniTygodnia.przestoje_so = godziny.przestoje_so;
            dniTygodnia.przestoje_ni = godziny.przestoje_ni;
            dniTygodnia.dojazd_pn = godziny.dojazd_pn;
            dniTygodnia.dojazd_wt = godziny.dojazd_wt;
            dniTygodnia.dojazd_sr = godziny.dojazd_sr;
            dniTygodnia.dojazd_cz = godziny.dojazd_cz;
            dniTygodnia.dojazd_pi = godziny.dojazd_pi;
            dniTygodnia.dojazd_so = godziny.dojazd_so;
            dniTygodnia.dojazd_ni = godziny.dojazd_ni;
            dniTygodnia.dieta_pn = Convert.ToInt32(godziny.dieta_pn);
            dniTygodnia.dieta_wt = Convert.ToInt32(godziny.dieta_wt);
            dniTygodnia.dieta_sr = Convert.ToInt32(godziny.dieta_sr);
            dniTygodnia.dieta_cz = Convert.ToInt32(godziny.dieta_cz);
            dniTygodnia.dieta_pi = Convert.ToInt32(godziny.dieta_pi);
            dniTygodnia.dieta_so = Convert.ToInt32(godziny.dieta_so);
            dniTygodnia.dieta_ni = Convert.ToInt32(godziny.dieta_ni);

            return dniTygodnia;
        }

        public static DniTygodnia GetUserSettings(int idusera)
        {
            DniTygodnia dniTygodnia = new DniTygodnia();
            bool sciagnieto = false;

            using (SqlConnection connection = new SqlConnection(
              connectionString))
            {

                string queryString = string.Format("select * from AspNetUserSettings WHERE Id = {0}", idusera);
                SqlCommand command = new SqlCommand(queryString, connection);
                connection.Open();
                SqlDataReader reader = command.ExecuteReader();
                try
                {
                    while (reader.Read())
                    {
                        sciagnieto = true;
                        dniTygodnia.od_pn = IsEmptyOd(reader[1].ToString());
                        dniTygodnia.od_wt = IsEmptyOd(reader[2].ToString());
                        dniTygodnia.od_sr = IsEmptyOd(reader[3].ToString());
                        dniTygodnia.od_cz = IsEmptyOd(reader[4].ToString());
                        dniTygodnia.od_pi = IsEmptyOd(reader[5].ToString());
                        dniTygodnia.od_so = IsEmptyOd(reader[6].ToString());
                        dniTygodnia.od_ni = IsEmptyOd(reader[7].ToString());
                        dniTygodnia.do_pn = IsEmptyDo(reader[8].ToString());
                        dniTygodnia.do_wt = IsEmptyDo(reader[9].ToString());
                        dniTygodnia.do_sr = IsEmptyDo(reader[10].ToString());
                        dniTygodnia.do_cz = IsEmptyDo(reader[11].ToString());
                        dniTygodnia.do_pi = IsEmptyDo(reader[12].ToString());
                        dniTygodnia.do_so = IsEmptyDo(reader[13].ToString());
                        dniTygodnia.do_ni = IsEmptyDo(reader[14].ToString());
                        dniTygodnia.przestoje_pn = IsEmptyPrzestoje(reader[15].ToString());
                        dniTygodnia.przestoje_wt = IsEmptyPrzestoje(reader[16].ToString());
                        dniTygodnia.przestoje_sr = IsEmptyPrzestoje(reader[17].ToString());
                        dniTygodnia.przestoje_cz = IsEmptyPrzestoje(reader[18].ToString());
                        dniTygodnia.przestoje_pi = IsEmptyPrzestoje(reader[19].ToString());
                        dniTygodnia.przestoje_so = IsEmptyPrzestoje(reader[20].ToString());
                        dniTygodnia.przestoje_ni = IsEmptyPrzestoje(reader[21].ToString());
                        dniTygodnia.dojazd_pn = IsEmptyDojazd(reader[22].ToString());
                        dniTygodnia.dojazd_wt = IsEmptyDojazd(reader[23].ToString());
                        dniTygodnia.dojazd_sr = IsEmptyDojazd(reader[24].ToString());
                        dniTygodnia.dojazd_cz = IsEmptyDojazd(reader[25].ToString());
                        dniTygodnia.dojazd_pi = IsEmptyDojazd(reader[26].ToString());
                        dniTygodnia.dojazd_so = IsEmptyDojazd(reader[27].ToString());
                        dniTygodnia.dojazd_ni = IsEmptyDojazd(reader[28].ToString());
                        dniTygodnia.dieta_pn = IsEmptyDieta(reader[29]);
                        dniTygodnia.dieta_wt = IsEmptyDieta(reader[30]);
                        dniTygodnia.dieta_sr = IsEmptyDieta(reader[31]);
                        dniTygodnia.dieta_cz = IsEmptyDieta(reader[32]);
                        dniTygodnia.dieta_pi = IsEmptyDieta(reader[33]);
                        dniTygodnia.dieta_so = IsEmptyDieta(reader[34]);
                        dniTygodnia.dieta_ni = IsEmptyDieta(reader[35]);
                    }
                }
                catch (Exception e)
                {
                    Debug.WriteLine(e.Message);
                }
                finally
                {
                    // Always call Close when done reading.
                    reader.Close();
                }

                if (!sciagnieto)
                {
                    dniTygodnia.od_pn = IsEmptyOd("");
                    dniTygodnia.od_wt = IsEmptyOd("");
                    dniTygodnia.od_sr = IsEmptyOd("");
                    dniTygodnia.od_cz = IsEmptyOd("");
                    dniTygodnia.od_pi = IsEmptyOd("");
                    dniTygodnia.od_so = IsEmptyOd("");
                    dniTygodnia.od_ni = IsEmptyOd("");
                    dniTygodnia.do_pn = IsEmptyDo("");
                    dniTygodnia.do_wt = IsEmptyDo("");
                    dniTygodnia.do_sr = IsEmptyDo("");
                    dniTygodnia.do_cz = IsEmptyDo("");
                    dniTygodnia.do_pi = IsEmptyDo("");
                    dniTygodnia.do_so = IsEmptyDo("");
                    dniTygodnia.do_ni = IsEmptyDo("");
                    dniTygodnia.przestoje_pn = IsEmptyPrzestoje("");
                    dniTygodnia.przestoje_wt = IsEmptyPrzestoje("");
                    dniTygodnia.przestoje_sr = IsEmptyPrzestoje("");
                    dniTygodnia.przestoje_cz = IsEmptyPrzestoje("");
                    dniTygodnia.przestoje_pi = IsEmptyPrzestoje("");
                    dniTygodnia.przestoje_so = IsEmptyPrzestoje("");
                    dniTygodnia.przestoje_ni = IsEmptyPrzestoje("");
                    dniTygodnia.dojazd_pn = IsEmptyDojazd("");
                    dniTygodnia.dojazd_wt = IsEmptyDojazd("");
                    dniTygodnia.dojazd_sr = IsEmptyDojazd("");
                    dniTygodnia.dojazd_cz = IsEmptyDojazd("");
                    dniTygodnia.dojazd_pi = IsEmptyDojazd("");
                    dniTygodnia.dojazd_so = IsEmptyDojazd("");
                    dniTygodnia.dojazd_ni = IsEmptyDojazd("");
                    dniTygodnia.dieta_pn = IsEmptyDieta("");
                    dniTygodnia.dieta_wt = IsEmptyDieta("");
                    dniTygodnia.dieta_sr = IsEmptyDieta("");
                    dniTygodnia.dieta_cz = IsEmptyDieta("");
                    dniTygodnia.dieta_pi = IsEmptyDieta("");
                    dniTygodnia.dieta_so = IsEmptyDieta("");
                    dniTygodnia.dieta_ni = IsEmptyDieta("");
                }

                return dniTygodnia;
            }
        }

        public static int IsEmptyDieta(object v)
        {
            try
            {
                return Convert.ToInt32(v);
            }
            catch (Exception blad005)
            {
                Debug.WriteLine(blad005.Message);
                return 0;
            }
        }

        public static string IsEmptyDojazd(string v)
        {
            if (string.IsNullOrEmpty(v) || v == "0")
            {
                return "";
            }
            else
            {
                return v.Replace(" ", ""); ;
            }
        }

        public static string IsEmptyPrzestoje(string v)
        {
            if (string.IsNullOrEmpty(v) || v == "0")
            {
                return "";
            }
            else
            {
                return v.Replace(" ", ""); ;
            }
        }

        public static string IsEmptyDo(string v)
        {
            if (string.IsNullOrEmpty(v))
            {
                return "00.00";
            }
            else
            {
                return v;
            }
        }

        private static string IsEmptyOd(string v)
        {
            if (string.IsNullOrEmpty(v))
            {
                return "00.00";
            }
            else
            {
                return v;
            }
        }

        public static List<Brygada> GetBrygada(int id, bool calosc)
        {
            List<Brygada> brygada = new List<Brygada>();

            using (SqlConnection connection = new SqlConnection(
              connectionString))
            {

                string queryString = string.Format("SELECT nazwisko + ' ' + imie as nazwa, idpracownika, CASE stanowisko WHEN 'BRYGADZISTA/MAJSTER' THEN 1 WHEN 'ZASTĘPCA MAJSTRA' THEN 2 ELSE 3 END as stanowisko FROM pracownik WHERE pracuje = 1 AND (brygadzistaID = {0} OR idpracownika = {0})  order by 3 asc, 1", id);
                if (calosc)
                {
                    queryString = "SELECT nazwisko + ' ' + imie, idpracownika FROM pracownik WHERE pracuje = 1 AND brygadzistaId <> 0 order by 1";
                }
                SqlCommand command = new SqlCommand(queryString, connection);
                connection.Open();
                SqlDataReader reader = command.ExecuteReader();
                try
                {
                    while (reader.Read())
                    {
                        string nazwa = reader[0].ToString();
                        int idpracownika = Convert.ToInt32(reader[1]);
                        brygada.Add(new Brygada(nazwa, idpracownika, GetBrygadzistaNazwa(idpracownika), GetBrygadzistaID(idpracownika)));
                    }
                }
                finally
                {
                    // Always call Close when done reading.
                    reader.Close();
                }
            }

            return brygada;
        }

        public static List<Brygada> GetBrygadaZGodzin(int brygadzistaID, Budowa currentBudowa, Tydzien tydzien)
        {
            pobiNTRANETDataContext dc = new pobiNTRANETDataContext();
            var godziny = dc.AspNetGodzinies.Where(q => q.brygadzistaID == brygadzistaID && q.budowaID == currentBudowa.Id && q.rokTydzien == tydzien.getRokTydzien());
            List<Brygada> brygada = new List<Brygada>();
            foreach (var godz in godziny)
            {
                var pracownik = dc.pracowniks.Where(p => p.idpracownika == godz.pracownikID).First();
                brygada.Add(new Brygada(pracownik.nazwisko + ' ' + pracownik.imie, pracownik.idpracownika, GetBrygadzistaNazwa(pracownik.idpracownika), GetBrygadzistaID(pracownik.idpracownika)));
            }

            return brygada;
        }

        public static List<Brygada> GetBrygadaZGodzin(Budowa currentBudowa, Tydzien tydzien)
        {
            pobiNTRANETDataContext dc = new pobiNTRANETDataContext();
            var godziny = dc.AspNetGodzinies.Where(q => q.budowaID == currentBudowa.Id && q.rokTydzien == tydzien.getRokTydzien());
            List<Brygada> brygada = new List<Brygada>();
            foreach (var godz in godziny)
            {
                var pracownik = dc.pracowniks.Where(p => p.idpracownika == godz.pracownikID).First();
                brygada.Add(new Brygada(pracownik.nazwisko + ' ' + pracownik.imie, pracownik.idpracownika, GetBrygadzistaNazwa(pracownik.idpracownika), GetBrygadzistaID(pracownik.idpracownika)));
            }

            return brygada;
        }

        public static List<AspNetGodziny> GetGodzinyDoPogladu(string user)
        {
            pobiNTRANETDataContext dc = new pobiNTRANETDataContext();
            int id_kierownika = Help.GetIdIntranetFromDb(user);
            var godziny = dc.AspNetGodzinies.Where(q => q.brygadzistaID == id_kierownika).ToList();
            if (Help.SprawdzCzyAdmin(user))
            {
                godziny = dc.AspNetGodzinies.ToList();
            }

            return godziny.OrderByDescending(q => q.rokTydzien).ThenBy(q => q.brygadzistaID).ThenBy(q => q.budowaID).ToList();
        }

        public static Budowa GetBudowa(int id)
        {
            if (id != 0)
            {
                pobiNTRANETDataContext dc = new pobiNTRANETDataContext();
                var projekt = dc.projekts.FirstOrDefault(p => p.idprojektu == id);
                Budowa budowa = new Budowa(projekt.idprojektu, string.Format("[{0}] {1}", projekt.numer, projekt.nazwa), projekt.numer.ToString());
                return budowa;
            }
            else
            {
                return new Budowa(0, "", "");
            }
        }

        public static List<Budowa> GetBudowy()
        {
            List<Budowa> listaBudow = new List<Budowa>();

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                string queryString = string.Format("SELECT idprojektu, '[' + CAST(numer AS nvarchar) + '] ' + nazwa AS budowa, numer, LEN(numer) as dlugosc FROM projekt WHERE LEN(numer) <> 1 ORDER BY LEN(numer), numer DESC");
                SqlCommand command = new SqlCommand(queryString, connection);
                connection.Open();
                SqlDataReader reader = command.ExecuteReader();
                try
                {
                    while (reader.Read())
                    {
                        Budowa budowa = new Budowa(Convert.ToInt32(reader[0]), reader[1].ToString(), reader[2].ToString());
                        listaBudow.Add(budowa);
                    }
                }
                finally
                {
                    // Always call Close when done reading.
                    reader.Close();
                }
            }

            return listaBudow;
        }

        public static List<Budowa> GetBudowyZGodzin(List<AspNetGodziny> godziny)
        {
            List<Budowa> listaBudow = new List<Budowa>();

            var listaId = godziny.OrderBy(g => GetBudowa(g.budowaID).Numer.Length).ThenByDescending(g => GetBudowa(g.budowaID).Numer).Select(g => g.budowaID).Distinct().ToList();
            foreach (var obj in listaId)
            {
                if (obj != 0)
                {
                    Budowa budowa = new Budowa(obj, GetNazwaBudowy(obj, true), GetNumerBudowy(obj));
                    listaBudow.Add(budowa);
                }
            }

            return listaBudow;
        }

        public static Tydzien GetTydzien(int tydzien)
        {
            if (tydzien != 0)
            {
                Tydzien week = null;
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    string queryString = string.Format("SELECT * FROM AspNetLata WHERE idtygodnia = {0}", tydzien);
                    SqlCommand command = new SqlCommand(queryString, connection);
                    connection.Open();
                    SqlDataReader reader = command.ExecuteReader();
                    try
                    {
                        while (reader.Read())
                        {
                            week = new Tydzien(Convert.ToInt32(reader[0]), reader[1].ToString(), Convert.ToInt32(reader[2]), Convert.ToInt32(reader[3]));
                        }
                    }
                    finally
                    {
                        // Always call Close when done reading.
                        reader.Close();
                    }
                }
                return week;
            }
            else
            {
                return new Tydzien(0, "", 0, 0);
            }
        }

        public static Tydzien GetTydzien(string tydzien)
        {
            Tydzien week = null;
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                string queryString = string.Format("SELECT * FROM AspNetLata WHERE rokTydzien = '{0}'", tydzien);
                SqlCommand command = new SqlCommand(queryString, connection);
                connection.Open();
                SqlDataReader reader = command.ExecuteReader();
                try
                {
                    while (reader.Read())
                    {
                        week = new Tydzien(Convert.ToInt32(reader[0]), reader[1].ToString(), Convert.ToInt32(reader[2]), Convert.ToInt32(reader[3]));
                    }
                }
                finally
                {
                    // Always call Close when done reading.
                    reader.Close();
                }
            }
            return week;
        }

        public static void SaveSettings(int userId, DniTygodnia s)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                string queryString1 = string.Format("IF EXISTS (SELECT * FROM AspNetUserSettings WHERE Id = {7}) UPDATE AspNetUserSettings SET od_pn = '{0}', od_wt = '{1}', od_sr = '{2}', od_cz = '{3}', od_pi = '{4}', od_so = '{5}', od_ni = '{6}' WHERE Id = {7} ELSE INSERT INTO AspNetUserSettings(od_pn, od_wt, od_sr, od_cz, od_pi, od_so, od_ni, Id) VALUES('{0}','{1}','{2}','{3}','{4}','{5}','{6}',{7})", s.od_pn, s.od_wt, s.od_sr, s.od_cz, s.od_pi, s.od_so, s.od_ni, userId);
                SqlCommand command1 = new SqlCommand(queryString1, connection);
                connection.Open();
                command1.ExecuteNonQuery();
                connection.Close();

                string queryString2 = string.Format("IF EXISTS (SELECT * FROM AspNetUserSettings WHERE Id = {7}) UPDATE AspNetUserSettings SET do_pn = '{0}', do_wt = '{1}', do_sr = '{2}', do_cz = '{3}', do_pi = '{4}', do_so = '{5}', do_ni = '{6}' WHERE Id = {7} ELSE INSERT INTO AspNetUserSettings(do_pn, do_wt, do_sr, do_cz, do_pi, do_so, do_ni, Id) VALUES('{0}','{1}','{2}','{3}','{4}','{5}','{6}',{7})", s.do_pn, s.do_wt, s.do_sr, s.do_cz, s.do_pi, s.do_so, s.do_ni, userId);
                SqlCommand command2 = new SqlCommand(queryString2, connection);
                connection.Open();
                command2.ExecuteNonQuery();
                connection.Close();

                string queryString3 = string.Format("IF EXISTS (SELECT * FROM AspNetUserSettings WHERE Id = {7}) UPDATE AspNetUserSettings SET przestoje_pn = '{0}', przestoje_wt = '{1}', przestoje_sr = '{2}', przestoje_cz = '{3}', przestoje_pi = '{4}', przestoje_so = '{5}', przestoje_ni = '{6}' WHERE Id = {7} ELSE INSERT INTO AspNetUserSettings(przestoje_pn, przestoje_wt, przestoje_sr, przestoje_cz, przestoje_pi, przestoje_so, przestoje_ni, Id) VALUES('{0}','{1}','{2}','{3}','{4}','{5}','{6}',{7})", s.przestoje_pn, s.przestoje_wt, s.przestoje_sr, s.przestoje_cz, s.przestoje_pi, s.przestoje_so, s.przestoje_ni, userId);
                SqlCommand command3 = new SqlCommand(queryString3, connection);
                connection.Open();
                command3.ExecuteNonQuery();
                connection.Close();

                string queryString4 = string.Format("IF EXISTS (SELECT * FROM AspNetUserSettings WHERE Id = {7}) UPDATE AspNetUserSettings SET dojazd_pn = '{0}', dojazd_wt = '{1}', dojazd_sr = '{2}', dojazd_cz = '{3}', dojazd_pi = '{4}', dojazd_so = '{5}', dojazd_ni = '{6}' WHERE Id = {7} ELSE INSERT INTO AspNetUserSettings(dojazd_pn, dojazd_wt, dojazd_sr, dojazd_cz, dojazd_pi, dojazd_so, dojazd_ni, Id) VALUES('{0}','{1}','{2}','{3}','{4}','{5}','{6}',{7})", s.dojazd_pn, s.dojazd_wt, s.dojazd_sr, s.dojazd_cz, s.dojazd_pi, s.dojazd_so, s.dojazd_ni, userId);
                SqlCommand command4 = new SqlCommand(queryString4, connection);
                connection.Open();
                command4.ExecuteNonQuery();
                connection.Close();

                string queryString5 = string.Format("IF EXISTS (SELECT * FROM AspNetUserSettings WHERE Id = {7}) UPDATE AspNetUserSettings SET dieta_pn = '{0}', dieta_wt = '{1}', dieta_sr = '{2}', dieta_cz = '{3}', dieta_pi = '{4}', dieta_so = '{5}', dieta_ni = '{6}' WHERE Id = {7} ELSE INSERT INTO AspNetUserSettings(dieta_pn, dieta_wt, dieta_sr, dieta_cz, dieta_pi, dieta_so, dieta_ni, Id) VALUES('{0}','{1}','{2}','{3}','{4}','{5}','{6}',{7})", s.dieta_pn, s.dieta_wt, s.dieta_sr, s.dieta_cz, s.dieta_pi, s.dieta_so, s.dieta_ni, userId);
                SqlCommand command5 = new SqlCommand(queryString5, connection);
                connection.Open();
                command5.ExecuteNonQuery();
                connection.Close();
            }
        }

        public static string GetDayOfWeek(string week, int dayOfWeek)
        {
            string result = "";

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                string queryString = string.Format("SELECT TOP 1 data, dzien_tygodnia FROM lata WHERE rokTydzien = '{0}' and dzien_tygodnia = {1} ORDER BY dzien_tygodnia desc", week, dayOfWeek);
                SqlCommand command = new SqlCommand(queryString, connection);
                connection.Open();
                SqlDataReader reader = command.ExecuteReader();
                try
                {
                    while (reader.Read())
                    {
                        result = reader[0].ToString();
                    }
                }
                finally
                {
                    // Always call Close when done reading.
                    reader.Close();
                }
            }

            try
            {
                return DateTime.Parse(result).ToString("dd-MM-yyyy");
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
                if (string.IsNullOrEmpty(week))
                {
                    return "31-12-" + DateTime.Now.Year;
                }
                return string.Format("31-12-{0}", week.Substring(0, 4));
            }
        }

        public static List<Tydzien> GetTygodnie(int year)
        {
            List<Tydzien> listaTygodni = new List<Tydzien>();

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                string queryString = string.Format("SELECT * FROM AspNetLata WHERE zablokowany <> 1 and rok = {0}", year.ToString());
                SqlCommand command = new SqlCommand(queryString, connection);
                connection.Open();
                SqlDataReader reader = command.ExecuteReader();
                try
                {
                    while (reader.Read())
                    {
                        Tydzien tydzien = new Tydzien(Convert.ToInt32(reader[0]), reader[1].ToString(), Convert.ToInt32(reader[2]), Convert.ToInt32(reader[3]));
                        listaTygodni.Add(tydzien);
                    }
                }
                finally
                {
                    // Always call Close when done reading.
                    reader.Close();
                }
            }

            return listaTygodni;
        }

        public static List<Tydzien> GetTygodnieCalosc(int year)
        {
            List<Tydzien> listaTygodni = new List<Tydzien>();

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                string queryString = string.Format("SELECT * FROM AspNetLata WHERE rok = {0}", year.ToString());
                SqlCommand command = new SqlCommand(queryString, connection);
                connection.Open();
                SqlDataReader reader = command.ExecuteReader();
                try
                {
                    while (reader.Read())
                    {
                        Tydzien tydzien = new Tydzien(Convert.ToInt32(reader[0]), reader[1].ToString(), Convert.ToInt32(reader[2]), Convert.ToInt32(reader[3]));
                        listaTygodni.Add(tydzien);
                    }
                }
                finally
                {
                    // Always call Close when done reading.
                    reader.Close();
                }
            }

            return listaTygodni;
        }

        public static List<Tydzien> GetTygodnieZablokowaneLubNie(int year)
        {
            List<Tydzien> listaTygodni = new List<Tydzien>();

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                string queryString = string.Format("SELECT * FROM AspNetLata");
                SqlCommand command = new SqlCommand(queryString, connection);
                connection.Open();
                SqlDataReader reader = command.ExecuteReader();
                try
                {
                    while (reader.Read())
                    {
                        Tydzien tydzien = new Tydzien(Convert.ToInt32(reader[0]), reader[1].ToString(), Convert.ToInt32(reader[2]), Convert.ToInt32(reader[3]));
                        listaTygodni.Add(tydzien);
                    }
                }
                finally
                {
                    // Always call Close when done reading.
                    reader.Close();
                }
            }

            return listaTygodni;
        }

        public static int GetBrygadzistaID(int answer)
        {
            int brygadzista = 0;
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                string queryString = string.Format("SELECT brygadzistaId FROM pracownik p WHERE idpracownika = {0}", answer);
                SqlCommand command = new SqlCommand(queryString, connection);
                connection.Open();
                SqlDataReader reader = command.ExecuteReader();
                try
                {
                    while (reader.Read())
                    {
                        brygadzista = Convert.ToInt32(reader[0]);
                    }
                }
                finally
                {
                    // Always call Close when done reading.
                    reader.Close();
                }
            }
            return brygadzista;
        }

        public static string GetBrygadzistaNazwa(int answer)
        {
            string brygadzista = "";
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                string queryString = string.Format("SELECT (SELECT nazwisko + ' ' + imie FROM pracownik WHERE idpracownika = p.brygadzistaID) FROM pracownik p WHERE idpracownika = {0}", answer);
                SqlCommand command = new SqlCommand(queryString, connection);
                connection.Open();
                SqlDataReader reader = command.ExecuteReader();
                try
                {
                    while (reader.Read())
                    {
                        brygadzista = reader[0].ToString();
                    }
                }
                finally
                {
                    // Always call Close when done reading.
                    reader.Close();
                }
            }
            return brygadzista;
        }

        public static List<Brygadzista> GetBrygadzistow(bool calosc)
        {
            List<Brygadzista> listaBrygadzistow = new List<Brygadzista>();
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                string queryString = string.Format("SELECT nazwisko + ' ' + imie, idpracownika FROM pracownik WHERE stanowisko = 'BRYGADZISTA/MAJSTER' and NOT EXISTS(SELECT id_intranet FROM AspNetUsers where id_intranet = pracownik.idpracownika) and pracuje = 1 order by 1");
                if (calosc)
                {
                    queryString = string.Format("SELECT nazwisko + ' ' + imie, idpracownika FROM pracownik WHERE stanowisko = 'BRYGADZISTA/MAJSTER' and pracuje = 1 order by 1");
                }
                SqlCommand command = new SqlCommand(queryString, connection);
                connection.Open();
                SqlDataReader reader = command.ExecuteReader();
                try
                {
                    while (reader.Read())
                    {
                        listaBrygadzistow.Add(new Brygadzista(Convert.ToInt32(reader[1]), reader[0].ToString()));
                    }
                }
                finally
                {
                    // Always call Close when done reading.
                    reader.Close();
                }
            }
            return listaBrygadzistow;
        }

        public static List<Brygadzista> GetBrygadzistowZGodzin(List<AspNetGodziny> godziny)
        {
            List<Brygadzista> listaBrygadzistow = new List<Brygadzista>();

            var listaId = godziny.OrderBy(g => GetBrygadzistaPracownikNazwa(g.brygadzistaID)).Select(g => g.brygadzistaID).Distinct().ToList();
            foreach (var obj in listaId)
            {
                Brygadzista brygadzista = new Brygadzista(obj, GetNameAndSurname(obj));
                listaBrygadzistow.Add(brygadzista);
            }

            return listaBrygadzistow;
        }

        public static List<Brygadzista> GetBrygadzistowDoAkceptacji()
        {
            List<Brygadzista> listaBrygadzistow = new List<Brygadzista>();

            pobiNTRANETDataContext dc = new pobiNTRANETDataContext();
            List<int> list = dc.AspNetGodzinies.Where(g => g.export != 1 && g.export != -1).Select(g => g.brygadzistaID).Distinct().ToList();
            //.Where(g => g.brygadzistaID != g.pracownikID)

            foreach (var l in list)
            {
                Brygadzista brygadzista = new Brygadzista(GetBrygadzistaPracownikId(l), GetBrygadzistaPracownikNazwa(l));
                listaBrygadzistow.Add(brygadzista);
            }


            return listaBrygadzistow;
        }

        private static int GetBrygadzistaPracownikId(int id)
        {
            pobiNTRANETDataContext dc = new pobiNTRANETDataContext();
            return dc.pracowniks.First(p => p.idpracownika == id).idpracownika;
        }

        private static string GetBrygadzistaPracownikNazwa(int id)
        {
            pobiNTRANETDataContext dc = new pobiNTRANETDataContext();
            var pracownik = dc.pracowniks.First(p => p.idpracownika == id);
            return pracownik.imie + " " + pracownik.nazwisko;
        }

        public static List<Brygada> NowaBrygadaDelete(int idpracownika, List<Brygada> brygada)
        {
            var nowaBrygada = new List<Brygada>();
            foreach (var pracownik in brygada)
            {
                if (pracownik.getId() != idpracownika)
                {
                    nowaBrygada.Add(pracownik);
                }
            }
            return nowaBrygada;
        }

        public static string GetCurrentWeek()
        {
            string month = DateTime.Now.Month.ToString();
            string year = DateTime.Now.Year.ToString();
            string day = DateTime.Now.Day.ToString();
            string currentWeek = "";
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                string queryString = string.Format("SELECT rokTydzien FROM lata where rok = {0} and miesiac = {1} and dzien_miesiaca = {2} ", year, month, day);
                SqlCommand command = new SqlCommand(queryString, connection);
                connection.Open();
                SqlDataReader reader = command.ExecuteReader();
                try
                {
                    while (reader.Read())
                    {
                        currentWeek = reader[0].ToString();
                    }
                }
                finally
                {
                    // Always call Close when done reading.
                    reader.Close();
                }
            }
            return currentWeek;
        }

        public static int GetIdIntranetFromDb(string name)
        {
            int id_intranet = 0;
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                string queryString = "SELECT id_intranet FROM AspNetUsers WHERE email = '" + name + "'";
                SqlCommand command = new SqlCommand(queryString, connection);
                connection.Open();
                SqlDataReader reader = command.ExecuteReader();
                try
                {
                    while (reader.Read())
                    {
                        id_intranet = Convert.ToInt32(reader[0]);
                    }
                }
                finally
                {
                    // Always call Close when done reading.
                    reader.Close();
                }
            }

            return id_intranet;
        }

        public static string GetNumerBudowy(int id)
        {
            pobiNTRANETDataContext dc = new pobiNTRANETDataContext();
            return dc.projekts.Where(p => p.idprojektu == id).Select(p => string.Format("{0}", p.numer)).First().ToString();
        }

        public static string GetNazwaBudowy(int id, bool calosc)
        {
            try
            {
                pobiNTRANETDataContext dc = new pobiNTRANETDataContext();
                if (calosc)
                    return dc.projekts.Where(p => p.idprojektu == id).Select(p => string.Format("[{0}]{1}", p.numer, p.nazwa)).First().ToString();
                else
                    return dc.projekts.Where(p => p.idprojektu == id).Select(p => string.Format("[{0}]{1}{2}", p.numer, p.nazwa.Substring(0, 30), p.nazwa.Length > 30 ? "..." : "")).First().ToString();
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
                return "BUDOWA SERWISOWA";
            }
        }

        public static string GetObecny(string obecny)
        {
            switch (obecny)
            {
                case "KR":
                    return "Chorobowe";
                case "FU":
                    return "Nieob. nieuspr.";
                case "U":
                    return "Urlop";
                case "UU":
                    return "Urlop bezpł.";
                case "SU":
                    return "Urlop okol.";
                default:
                    return "Obecny";
            }
        }

        public static string GetNameAndSurname(int id)
        {
            string nazwa = "";
            using (SqlConnection connection = new SqlConnection(
              connectionString))
            {
                string queryString = "SELECT nazwisko + ' ' + imie FROM pracownik WHERE idpracownika = " + id;
                SqlCommand command = new SqlCommand(queryString, connection);
                connection.Open();
                SqlDataReader reader = command.ExecuteReader();
                try
                {
                    while (reader.Read())
                    {
                        nazwa = reader[0].ToString();
                    }
                }
                finally
                {
                    // Always call Close when done reading.
                    reader.Close();
                }
            }

            return nazwa;
        }

        public static bool isSession(object session)
        {
            if (session == null)
            {
                return false;
            }
            else if (session.GetType().IsGenericType)
            {
                if (session.GetType().GetGenericTypeDefinition().IsAssignableFrom(typeof(Dictionary<,>)) || session.GetType().GetGenericTypeDefinition().IsAssignableFrom(typeof(List<>)))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else if (Convert.ToInt32(session) == 0 || session.ToString() == "")
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public static IHtmlString SerializeObject(object value)
        {
            using (var stringWriter = new StringWriter())
            using (var jsonWriter = new JsonTextWriter(stringWriter))
            {
                var serializer = new JsonSerializer
                {
                    // Let's use camelCasing as is common practice in JavaScript
                    ContractResolver = new CamelCasePropertyNamesContractResolver()
                };

                // We don't want quotes around object names
                jsonWriter.QuoteName = false;
                serializer.Serialize(jsonWriter, value);

                var str = stringWriter.ToString();

                return new HtmlString(str);
            }
        }

        public static string GetDieta(int value)
        {
            switch (value)
            {
                case 1: return "PL";
                case 2: return "EU";
                default: return "BRAK";
            }
        }
    }
}
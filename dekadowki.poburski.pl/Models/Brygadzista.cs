﻿using System.ComponentModel.DataAnnotations;

namespace dekadowki.poburski.pl.Models
{
    public class Brygadzista
    {
        public int id { get; set; }
        [Display(Name = "Brygadzista")]
        public string nazwa { get; set; }

        public int GetId()
        {
            return this.id;
        }

        public string GetName()
        {
            return this.nazwa;
        }

        public Brygadzista(int id, string nazwa)
        {
            this.id = id;
            this.nazwa = nazwa;
        }
    }
}
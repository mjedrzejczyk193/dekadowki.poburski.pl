﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(dekadowki.poburski.pl.Startup))]
namespace dekadowki.poburski.pl
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
